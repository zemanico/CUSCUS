/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Marco Miozzo <marco.miozzo@cttc.es>
 *         Nicola Baldo <nbaldo@cttc.es>
 * 
 */

#include <cmath>

#include "ns3/log.h"
#include "ns3/mobility-model.h"
#include "ns3/double.h"
#include "ns3/pointer.h"
#include "ns3/okumura-hata-propagation-loss-model.h"
#include "ns3/itu-r-1411-los-propagation-loss-model.h"
#include "ns3/itu-r-1411-nlos-over-rooftop-propagation-loss-model.h"
#include "ns3/itu-r-1238-propagation-loss-model.h"
#include "ns3/kun-2600-mhz-propagation-loss-model.h"
#include <ns3/mobility-building-info.h>
#include "ns3/enum.h"

#include "hybrid-raylight-buildings-propagation-loss-model.h"


namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("HybridRaylightBuildingsPropagationLossModel");

NS_OBJECT_ENSURE_REGISTERED (HybridRaylightBuildingsPropagationLossModel);



HybridRaylightBuildingsPropagationLossModel::HybridRaylightBuildingsPropagationLossModel ()
{
  m_okumuraHata = CreateObject<OkumuraHataPropagationLossModel> ();
  m_ituR1411Los = CreateObject<ItuR1411LosPropagationLossModel> ();
  m_ituR1411NlosOverRooftop = CreateObject<ItuR1411NlosOverRooftopPropagationLossModel> ();
  m_ituR1238 = CreateObject<ItuR1238PropagationLossModel> ();
  m_kun2600Mhz = CreateObject<Kun2600MhzPropagationLossModel> ();
}

HybridRaylightBuildingsPropagationLossModel::~HybridRaylightBuildingsPropagationLossModel ()
{
}

TypeId
HybridRaylightBuildingsPropagationLossModel::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::HybridRaylightBuildingsPropagationLossModel")
    
    .SetParent<BuildingsPropagationLossModel> ()
    
    .AddConstructor<HybridRaylightBuildingsPropagationLossModel> ()
    .SetGroupName ("Buildings")
    
    .AddAttribute ("Frequency",
                   "The Frequency  (default is 2.106 GHz).",
                   DoubleValue (2160e6),
                   MakeDoubleAccessor (&HybridRaylightBuildingsPropagationLossModel::SetFrequency),
                   MakeDoubleChecker<double> ())

    .AddAttribute ("Los2NlosThr",
                   " Threshold from LoS to NLoS in ITU 1411 [m].",
                   DoubleValue (200.0),
                   MakeDoubleAccessor (&HybridRaylightBuildingsPropagationLossModel::m_itu1411NlosThreshold),
                   MakeDoubleChecker<double> ())

    .AddAttribute ("Environment",
                   "Environment Scenario",
                   EnumValue (UrbanEnvironment),
                   MakeEnumAccessor (&HybridRaylightBuildingsPropagationLossModel::SetEnvironment),
                   MakeEnumChecker (UrbanEnvironment, "Urban",
                                    SubUrbanEnvironment, "SubUrban",
                                    OpenAreasEnvironment, "OpenAreas"))

    .AddAttribute ("CitySize",
                   "Dimension of the city",
                   EnumValue (LargeCity),
                   MakeEnumAccessor (&HybridRaylightBuildingsPropagationLossModel::SetCitySize),
                   MakeEnumChecker (SmallCity, "Small",
                                    MediumCity, "Medium",
                                    LargeCity, "Large"))

    .AddAttribute ("RooftopLevel",
                   "The height of the rooftop level in meters",
                   DoubleValue (20.0),
                   MakeDoubleAccessor (&HybridRaylightBuildingsPropagationLossModel::SetRooftopHeight),
                   MakeDoubleChecker<double> (0.0, 90.0))
                   
    .AddAttribute ("BuildingFileName",
				   "Path for the file containing the buildings.",
				   StringValue (""),
				   MakeStringAccessor (&HybridRaylightBuildingsPropagationLossModel::SetBuildingMap),
				   MakeStringChecker ())
				   
    .AddAttribute ("FixedExternalWallsType",
                   "The fixed type of material of which the external walls are made",
                   EnumValue (Building::StoneBlocks),
                   MakeEnumAccessor (&HybridRaylightBuildingsPropagationLossModel::GetFixedExtWallsType, &HybridRaylightBuildingsPropagationLossModel::SetFixedExtWallsType),
                   MakeEnumChecker (Building::Wood, "Wood",
                                    Building::ConcreteWithWindows, "ConcreteWithWindows",
                                    Building::ConcreteWithoutWindows, "ConcreteWithoutWindows",
                                    Building::StoneBlocks, "StoneBlocks"))

    ;
  
  return tid;
}


Building::ExtWallsType_t
HybridRaylightBuildingsPropagationLossModel::GetFixedExtWallsType () const
{
  return (m_fixedExternalWalls);
}

void 
HybridRaylightBuildingsPropagationLossModel::SetFixedExtWallsType (Building::ExtWallsType_t t)
{
  NS_LOG_FUNCTION (this << t);
  m_fixedExternalWalls = t;
}

void
HybridRaylightBuildingsPropagationLossModel::SetEnvironment (EnvironmentType env)
{
  m_okumuraHata->SetAttribute ("Environment", EnumValue (env));
  m_ituR1411NlosOverRooftop->SetAttribute ("Environment", EnumValue (env));
}

void
HybridRaylightBuildingsPropagationLossModel::SetCitySize (CitySize size)
{
  m_okumuraHata->SetAttribute ("CitySize", EnumValue (size));
  m_ituR1411NlosOverRooftop->SetAttribute ("CitySize", EnumValue (size));
}

void
HybridRaylightBuildingsPropagationLossModel::SetFrequency (double freq)
{
  m_okumuraHata->SetAttribute ("Frequency", DoubleValue (freq));
  m_ituR1411Los->SetAttribute ("Frequency", DoubleValue (freq));
  m_ituR1411NlosOverRooftop->SetAttribute ("Frequency", DoubleValue (freq));
  m_ituR1238->SetAttribute ("Frequency", DoubleValue (freq));
  m_frequency = freq;
}

void
HybridRaylightBuildingsPropagationLossModel::SetRooftopHeight (double rooftopHeight)
{
  m_rooftopHeight = rooftopHeight;
  m_ituR1411NlosOverRooftop->SetAttribute ("RooftopLevel", DoubleValue (rooftopHeight));
}


void
HybridRaylightBuildingsPropagationLossModel::SetBuildingMap (std::string mapFileName)
{
	if (mapFileName.size() > 0) {
		char inBuff[128];
		FILE *fbuilding = fopen(mapFileName.c_str(), "r");
		if (fbuilding) {
			while(fgets(inBuff, sizeof(inBuff), fbuilding)) {
				//double xmin, xmax, ymin, ymax, zmin, zmax;
				double dummy;
				buildingDef_t newB;
				
				sscanf(inBuff, "%lf,%lf;%lf,%lf;%lf,%lf\n", &newB.xmin, &newB.xmax, &newB.ymin, &newB.ymax, &dummy, &newB.h);
						
				//fprintf(stdout, "Adding building at  -->  %s", inBuff);
				
				bList.push_back(newB);
				
			}
			fclose(fbuilding);
		}
	}
}


double
HybridRaylightBuildingsPropagationLossModel::GetLoss (Ptr<MobilityModel> a, Ptr<MobilityModel> b) const
{
	if ((a->GetPosition ().z < 0) || (b->GetPosition ().z < 0)) {
		fprintf(stdout, "Underground node!!! [%lf;%lf;%lf] or [%lf;%lf;%lf]\n\n", 
			a->GetPosition ().x, a->GetPosition ().y, a->GetPosition ().z, 
			b->GetPosition ().x, b->GetPosition ().y, b->GetPosition ().z);
		fflush(stdout);
	}
  NS_ASSERT_MSG ((a->GetPosition ().z >= 0) && (b->GetPosition ().z >= 0), "HybridRaylightBuildingsPropagationLossModel does not support underground nodes (placed at z < 0)");

  
  double distance = a->GetDistanceFrom (b);

  // get the MobilityBuildingInfo pointers
  Ptr<MobilityBuildingInfo> a1 = a->GetObject<MobilityBuildingInfo> ();
  Ptr<MobilityBuildingInfo> b1 = b->GetObject<MobilityBuildingInfo> ();
  NS_ASSERT_MSG ((a1 != 0) && (b1 != 0), "HybridRaylightBuildingsPropagationLossModel only works with MobilityBuildingInfo");

  double loss = 0.0;

  if (a1->IsOutdoor ())
    {
      if (b1->IsOutdoor ())
        {
          if (distance > 1000)
            {
              NS_LOG_INFO (this << a->GetPosition ().z << b->GetPosition ().z << m_rooftopHeight);
              if ((a->GetPosition ().z < m_rooftopHeight)
                  && (b->GetPosition ().z < m_rooftopHeight))
                {
                  loss = ItuR1411 (a, b);
                  NS_LOG_INFO (this << " 0-0 (>1000): below rooftop -> ITUR1411 : " << loss);
                }
              else
                {
                  // Over the rooftop tranmission -> Okumura Hata
                  loss = OkumuraHata (a, b);
                  NS_LOG_INFO (this << " O-O (>1000): above rooftop -> OH : " << loss);
                }
            }
          else
            {
				int nWalls = countWallIntersectionNumber(a->GetPosition().x, a->GetPosition().y, a->GetPosition().z, 
					b->GetPosition().x, b->GetPosition().y, b->GetPosition().z);
					
				if (nWalls > 0) {
					printf("Intersection with %d walls\n", nWalls);
				}
              // short range outdoor communication
              loss = ItuR1411 (a, b) + (((double) nWalls) * ExternalWallLossType (m_fixedExternalWalls));
              NS_LOG_INFO (this << " 0-0 (<1000) Street canyon -> ITUR1411 : " << loss);
            }
        }
      else
        {
          // b indoor
          if (distance > 1000)
            {
              if ((a->GetPosition ().z < m_rooftopHeight)
                  && (b->GetPosition ().z < m_rooftopHeight))
                {                  
                  loss = ItuR1411 (a, b) + ExternalWallLoss (b1) + HeightLoss (b1);
                  NS_LOG_INFO (this << " 0-I (>1000): below rooftop -> ITUR1411 : " << loss);
                }
              else
                {
                  loss = OkumuraHata (a, b) + ExternalWallLoss (b1);
                  NS_LOG_INFO (this << " O-I (>1000): above the rooftop -> OH : " << loss);
                }
            }
          else
            {
              loss = ItuR1411 (a, b) + ExternalWallLoss (b1) + HeightLoss (b1);
              NS_LOG_INFO (this << " 0-I (<1000) ITUR1411 + BEL : " << loss);
            }
        } // end b1->isIndoor ()
    }
  else
    {
      // a is indoor
      if (b1->IsIndoor ())
        {
          if (a1->GetBuilding () == b1->GetBuilding ())
            {
              // nodes are in same building -> indoor communication ITU-R P.1238
              loss = ItuR1238 (a, b) + InternalWallsLoss (a1, b1);;
              NS_LOG_INFO (this << " I-I (same building) ITUR1238 : " << loss);

            }
          else
            {
              // nodes are in different buildings
              loss = ItuR1411 (a, b) + ExternalWallLoss (a1) + ExternalWallLoss (b1);
              NS_LOG_INFO (this << " I-I (different) ITUR1238 + 2*BEL : " << loss);
            }
        }
      else
        {
          // b is outdoor
          if (distance > 1000)
            {
              if ((a->GetPosition ().z < m_rooftopHeight)
                  && (b->GetPosition ().z < m_rooftopHeight))
                {
                  loss = ItuR1411 (a, b) + ExternalWallLoss (a1) + HeightLoss (a1);
                  NS_LOG_INFO (this << " I-O (>1000): down rooftop -> ITUR1411 : " << loss);
                }
              else
                {
                  // above rooftop -> OH
                  loss = OkumuraHata (a, b) + ExternalWallLoss (a1) + HeightLoss (a1);
                  NS_LOG_INFO (this << " =I-O (>1000) over rooftop OH + BEL + HG: " << loss);
                }
            }
          else
            {
              loss = ItuR1411 (a, b) + ExternalWallLoss (a1)  + HeightLoss (a1);
              NS_LOG_INFO (this << " I-O (<1000)  ITUR1411 + BEL + HG: " << loss);
            }
        } // end b1->IsIndoor ()
    } // end a1->IsOutdoor ()

  loss = std::max (loss, 0.0);
  
  
  //fprintf(stdout, "Path loss from [%lf;%lf;%lf] to [%lf;%lf;%lf] -> %lf\n\n", 
  //  a->GetPosition ().x, a->GetPosition ().y, a->GetPosition ().z, b->GetPosition ().x, b->GetPosition ().y, b->GetPosition ().z, loss);fflush(stdout);

  return loss;
}


double
HybridRaylightBuildingsPropagationLossModel::OkumuraHata (Ptr<MobilityModel> a, Ptr<MobilityModel> b) const
{
  if (m_frequency <= 2.3e9)
    {
      return m_okumuraHata->GetLoss (a, b);
    }
  else
    {
      return m_kun2600Mhz->GetLoss (a, b);
    }
}

double
HybridRaylightBuildingsPropagationLossModel::ItuR1411 (Ptr<MobilityModel> a, Ptr<MobilityModel> b) const
{
  if (a->GetDistanceFrom (b) < m_itu1411NlosThreshold)
    {
      return (m_ituR1411Los->GetLoss (a, b));
    }
  else
    {
      return (m_ituR1411NlosOverRooftop->GetLoss (a, b));
    }
}

double
HybridRaylightBuildingsPropagationLossModel::ItuR1238 (Ptr<MobilityModel> a, Ptr<MobilityModel> b) const
{
  return m_ituR1238->GetLoss (a,b);
}

double
HybridRaylightBuildingsPropagationLossModel::ExternalWallLossType (Building::ExtWallsType_t wall_type) const
{
  double loss = 0.0;
  if (wall_type == Building::Wood)
    {
      loss = 4;
    }
  else if (wall_type == Building::ConcreteWithWindows)
    {
      loss = 7;
    }
  else if (wall_type == Building::ConcreteWithoutWindows)
    {
      loss = 15; // 10 ~ 20 dB
    }
  else if (wall_type == Building::StoneBlocks)
    {
      loss = 12;
    }
  return (loss);
}

int 
HybridRaylightBuildingsPropagationLossModel::countWallIntersectionNumber(double ax, double ay, double az, double bx, double by, double bz) const
{
	int ris = 0;
	
	for (auto actb = bList.begin(); actb != bList.end(); actb++) {
		//buildingDef_t *actb = &(*it);
		
		if ( (az < actb->h) && (bz < actb->h) ) {
			ris += ( (	(checkSegmentIntersection(ax,ay,bx,by, actb->xmin,actb->ymin,actb->xmax,actb->ymin)) || 
						(checkSegmentIntersection(actb->xmin,actb->ymin,actb->xmax,actb->ymin, ax,ay,bx,by)) ) ? 1 : 0);
						
			ris += ( (	(checkSegmentIntersection(ax,ay,bx,by, actb->xmin,actb->ymin,actb->xmin,actb->ymax)) || 
						(checkSegmentIntersection(actb->xmin,actb->ymin,actb->xmin,actb->ymax, ax,ay,bx,by)) ) ? 1 : 0);
			
			ris += ( (	(checkSegmentIntersection(ax,ay,bx,by, actb->xmax,actb->ymax,actb->xmax,actb->ymin)) || 
						(checkSegmentIntersection(actb->xmax,actb->ymax,actb->xmax,actb->ymin, ax,ay,bx,by)) ) ? 1 : 0);
			
			ris += ( (	(checkSegmentIntersection(ax,ay,bx,by, actb->xmax,actb->ymax,actb->xmin,actb->ymax)) || 
						(checkSegmentIntersection(actb->xmax,actb->ymax,actb->xmin,actb->ymax, ax,ay,bx,by)) ) ? 1 : 0);
		}
	}
	
	return ris;
}

bool 
HybridRaylightBuildingsPropagationLossModel::checkSegmentIntersection(double p0_x, double p0_y, double p1_x, double p1_y, 
    double p2_x, double p2_y, double p3_x, double p3_y) const
{
    double s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s_numer, t_numer, denom;
    
    s10_x = p1_x - p0_x;
    s10_y = p1_y - p0_y;
    s02_x = p0_x - p2_x;
    s02_y = p0_y - p2_y;
    
    s_numer = s10_x * s02_y - s10_y * s02_x;
    if (s_numer < 0)
        return false; // No collision
 
    s32_x = p3_x - p2_x;
    s32_y = p3_y - p2_y;
    t_numer = s32_x * s02_y - s32_y * s02_x;
    if (t_numer < 0)
        return false; // No collision
 
    denom = s10_x * s32_y - s32_x * s10_y;
    if (s_numer > denom || t_numer > denom)
        return false; // No collision
 
    // Collision detected
	return true;
}

/*
int get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y, 
    float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
{
    float s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s_numer, t_numer, denom, t;
    s10_x = p1_x - p0_x;
    s10_y = p1_y - p0_y;
    s02_x = p0_x - p2_x;
    s02_y = p0_y - p2_y;
 
    s_numer = s10_x * s02_y - s10_y * s02_x;
    if (s_numer < 0)
        return 0; // No collision
 
    s32_x = p3_x - p2_x;
    s32_y = p3_y - p2_y;
    t_numer = s32_x * s02_y - s32_y * s02_x;
    if (t_numer < 0)
        return 0; // No collision
 
    denom = s10_x * s32_y - s32_x * s10_y;
    if (s_numer > denom || t_numer > denom)
        return 0; // No collision
 
    // Collision detected
    t = t_numer / denom;
    if (i_x != NULL)
        *i_x = p0_x + (t * s10_x);
    if (i_y != NULL)
        *i_y = p0_y + (t * s10_y);
 
    return 1;
}
* */

} // namespace ns3
