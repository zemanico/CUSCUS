#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    m_workingDirectory = "../CUSCUS-APP/";
    m_scriptLocation = "../CUSCUS-APP/testScript.sh";

    createMenu();
    createExecutionButtons();
    createTextSpace();

    QVBoxLayout *mainLayout = new QVBoxLayout;

    mainLayout->setMenuBar(m_menuBar);
    mainLayout->addWidget(m_bigEditor);
    mainLayout->addWidget(m_executionButtons);
    setLayout(mainLayout);

    setWindowTitle(tr("Basic Layouts"));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::createTextSpace()
{
    m_bigEditor = new QTextBrowser();
    m_bigEditor->setPlainText(tr("Console Dump "
                               "..."));
}

void Widget::slotStdOutAvailable(){
    QByteArray bytes = m_theProcess->readAllStandardOutput();
    QStringList lines = QString(bytes).split("\n");
    foreach (QString line, lines) {
        m_bigEditor->append(line);
    }
}

void Widget::slotFinishedExecution(int i ,QProcess::ExitStatus es){
    m_bigEditor->append(QStringLiteral("Finished, exit status: %1. That's all!").arg(i) + QString(es));
}

void Widget::slotExecuteScript(bool executed)
{
    if (!executed){
        executed = true;
        m_execute_script_button->setText("Libero");

        m_theProcess = new QProcess();
        m_theProcess->setWorkingDirectory(m_workingDirectory);
        m_theProcess->setProcessChannelMode(QProcess::MergedChannels);
        m_theProcess->start(m_scriptLocation);
        m_bigEditor->append(QString("Executing script in ") + m_scriptLocation);
        connect( m_theProcess, SIGNAL(readyReadStandardOutput()), SLOT(slotStdOutAvailable()) );
        connect( m_theProcess, SIGNAL(finished(int,QProcess::ExitStatus)), SLOT(slotFinishedExecution(int,QProcess::ExitStatus)) );

//        if(!proc->waitForStarted()) //default wait time 30 sec
//            qWarning() << " cannot start process ";

//        int waitTime = 60000 ; //60 sec
//        if (!proc->waitForFinished(waitTime))
//                 qWarning() << "timeout .. ";

        m_execute_script_button->setText("Occupato");
        m_theProcess->waitForFinished();

        delete m_theProcess;
        executed = false;

        m_execute_script_button->setText("Libero");

    } else{

        m_execute_script_button->setText("Occupato");
    }
}


void Widget::createMenu()
{
    m_menuBar = new QMenuBar;
    m_fileMenu = new QMenu(tr("&File"), this);
    m_exitAction = m_fileMenu->addAction(tr("Eeeee&xit"));
    m_menuBar->addMenu(m_fileMenu);
    connect(m_exitAction, SIGNAL(triggered()), QApplication::instance(), SLOT (quit()));
}
void Widget::createExecutionButtons()
{
    m_executionButtons = new QGroupBox(tr("Horizontal layout"));

    QHBoxLayout *layout = new QHBoxLayout;

    m_execute_script_button = new QPushButton("Esegui Script", this);
    layout->addWidget(m_execute_script_button);

    m_button = new QPushButton("Quit", this);
    layout->addWidget(m_button);

    connect(m_button, SIGNAL (released()), QApplication::instance(), SLOT (quit()));
    connect(m_execute_script_button, SIGNAL (clicked(bool)), this, SLOT (slotExecuteScript(bool)) );

    m_executionButtons->setLayout(layout);
}
