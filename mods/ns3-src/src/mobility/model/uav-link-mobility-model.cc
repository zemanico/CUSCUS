/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#include <cmath>
#include "ns3/core-module.h"
#include "ns3/simulator.h"
#include "ns3/random-variable-stream.h"
#include "ns3/pointer.h"
#include "ns3/string.h"
#include "uav-link-mobility-model.h"
#include "position-allocator.h"

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED (UavLinkMobilityModel);

TypeId
UavLinkMobilityModel::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::UavLinkMobilityModel")
    .SetParent<MobilityModel> ()
    .SetGroupName ("Mobility")
	.AddConstructor<UavLinkMobilityModel> ()
	.AddAttribute ("SharedMemName",
				   "The name of the shared memory where to get the position.",
				   StringValue (""),
				   MakeStringAccessor (&UavLinkMobilityModel::shMemName),
				   MakeStringChecker ())
	.AddAttribute ("SharedMemSize",
				   "The size of the shared memory where to get the position.",
				   UintegerValue (0),
				   MakeUintegerAccessor (&UavLinkMobilityModel::shMemSize),
				   MakeUintegerChecker<uint32_t> ())
	.AddAttribute ("SharedMemStruct",
				   "The data structure used in shared memory.",
				   StringValue ("quaternion"),
				   MakeStringAccessor (&UavLinkMobilityModel::shMemStruct),
				   MakeStringChecker ())
	.AddAttribute ("PositionUpdateTime",
				   "Time interval to update the UAV position reading from the shared memory",
				   TimeValue (MilliSeconds (10)),
				   MakeTimeAccessor (&UavLinkMobilityModel::positionUpdateTime),
				   MakeTimeChecker ());

  return tid;
}

void UavLinkMobilityModel::UpdateActualPositionFromShMem (void)
{

	NS_ASSERT(mem_segment);

	if (shMemStruct.compare("quaternion") == 0) {
		uav_states_t uav_s;
		float roll, pitch, yaw;

		sem_wait(sem);
		memcpy((char*)&uav_s, mem_segment, sizeof(uav_states_t));
		sem_post(sem);
		
		//if (uav_s.z > 0) uav_s.z = 0;		// The node cannot be underground

		Quat2Euler(uav_s.q0, uav_s.q1, uav_s.q2, uav_s.q3, roll, pitch, yaw);

		//actualPosition = Vector(uav_s.x, uav_s.y, uav_s.z);
		//actualVelocity = Vector(uav_s.vx, uav_s.vy, uav_s.vz);
		//actualOrientation = Vector(roll, pitch, yaw);
		actualPosition = Vector(uav_s.y, -uav_s.x, -uav_s.z);
		actualVelocity = Vector(uav_s.vy, -uav_s.vx, -uav_s.vz);
		actualOrientation = Vector(roll, pitch, yaw);
	}
	else if (shMemStruct.compare("euler") == 0) {
		uav_states_euler_t uav_s;

		sem_wait(sem);
		memcpy((char*)&uav_s, mem_segment, sizeof(uav_states_euler_t));
		sem_post(sem);

		actualPosition = Vector(uav_s.x, uav_s.y, uav_s.z);
		actualVelocity = Vector(uav_s.vx, uav_s.vy, uav_s.vz);
		actualOrientation = Vector(uav_s.roll, uav_s.pitch, uav_s.yaw);
	}
	
/*	if ((rand()%100) == 0 ) {
		std::cout << Simulator::Now () << ", UavLinkMobilityModel reading from shmem " << shMemName <<
				", POS: x=" << actualPosition.x << ", y=" << actualPosition.y << ", z=" << actualPosition.z <<
				"; VEL:" << actualVelocity.x << ", y=" << actualVelocity.y << ", z=" << actualVelocity.z <<
				"; ANG:" << actualOrientation.x << ", y=" << actualOrientation.y << ", z=" << actualOrientation.z <<
				std::endl;
		fflush(stdout);
	}
*/
	
	NotifyCourseChange ();
}

void UavLinkMobilityModel::Quat2Euler(float q0, float q1, float q2, float q3, float &roll, float &pitch, float &yaw) {
	//TODO implement the conversion
	roll = 0.0;
	pitch = 0.0;
	yaw = 0.0;
}

Vector UavLinkMobilityModel::DoGetPosition (void) const
{
	return actualPosition;
}

void  UavLinkMobilityModel::DoSetPosition (const Vector &position) {}

Vector UavLinkMobilityModel::DoGetVelocity (void) const
{
	return actualVelocity;
}

void UavLinkMobilityModel::DoInitialize (void)
{
	DoInitializePrivate ();
	MobilityModel::DoInitialize ();
}

void UavLinkMobilityModel::DoDispose (void) {
	DoDisposePrivateSharedMemory();
	MobilityModel::DoDispose ();
}

void UavLinkMobilityModel::DoInitializePrivate (void)
{
	actualPosition = Vector(0.0, 0.0, 0.0);
	actualVelocity = Vector(0.0, 0.0, 0.0);
	actualOrientation = Vector(0.0, 0.0, 0.0);

	mem_segment = NULL;
	fd = 0;
	sem = NULL;

	NS_LOG_UNCOND ("Init of UavLinkMobilityModel. ShMemName: " << shMemName << "; ShMemSize: " << shMemSize << "; TimeUpdate: " << positionUpdateTime);

	if (shMemSize == 0) {
		if (shMemStruct.compare("euler")){
			shMemSize = sizeof(uav_states_euler_t);
		}
		else {
			shMemSize = sizeof(uav_states_t);
		}
		NS_LOG_UNCOND ("[ShMemSize] - Shared memory set to default value: " << shMemSize);
	}

	DoInitializePrivateSharedMemory();

	m_event = Simulator::Schedule (positionUpdateTime, &UavLinkMobilityModel::PollPosition, this);
}

void UavLinkMobilityModel::DoInitializePrivateSharedMemory (void) {
	NS_ASSERT((shMemSize > 0) && (shMemName.length() > 0));
	//NS_ASSERT(shMemSize >= sizeof(uav_states_t));

	shm_name="/" + shMemName;
	fd = shm_open(shm_name.c_str(), O_RDWR | O_CREAT, 0666);
	if (fd == -1) {
		NS_FATAL_ERROR("Error creating shared memory " << shm_name);
	}
	ftruncate(fd, shMemSize);

	sem_name = "/"  + shMemName + "_sem";
	sem = sem_open(sem_name.c_str(), O_CREAT, 0666, 1);
	if (sem == SEM_FAILED) {
		NS_FATAL_ERROR("Error creating semaphore " << sem_name);
	}

	mem_segment = (char*) mmap(NULL, shMemSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (mem_segment == MAP_FAILED) {
		NS_FATAL_ERROR("Failed to map memory of size " << shMemSize);
	}

}

void UavLinkMobilityModel::DoDisposePrivateSharedMemory (void) {

	int status = munmap(mem_segment, shMemSize);
	if(status!=0) {
		//self->Err("Failed to unmap memory (%s)\n",strerror(-status));
		NS_FATAL_ERROR("Failed to unmap memory " << shMemSize << " - " << strerror(-status));
	}

	status = close(fd);
	if(status!=0) {
		//self->Err("Failed to close file (%s)\n",strerror(-status));
		NS_FATAL_ERROR("Failed to close file (" << shMemSize << ") - " << strerror(-status));
	}

	//do not check erros as it can be done by another process
	status = shm_unlink(shm_name.c_str());/*
	    if(status!=0)
	    {
	        self->Err("Failed to unlink memory (%s)\n",strerror(-status));
	    }
	 */
	//do not check erros as it can be done by another process
	status = sem_unlink(sem_name.c_str());/*
	    if(status!=0)
	    {
	        self->Err("Failed to unlink semaphore (%s)\n",strerror(-status));
	    }*/

	status = sem_close(sem);
	if(status!=0) {
		//self->Err("Failed to close semaphore (%s)\n",strerror(-status));
		NS_FATAL_ERROR("Failed to close semaphore (" << sem_name << ") - " << strerror(-status));
	}
}

void UavLinkMobilityModel::PollPosition (void) {
	UpdateActualPositionFromShMem();

	m_event.Cancel ();
	m_event = Simulator::Schedule (positionUpdateTime, &UavLinkMobilityModel::PollPosition, this);
}

} // namespace ns3
