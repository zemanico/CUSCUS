#!/bin/bash

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# FUNCTION DEFINITION
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# define usage function
usage(){
    
    echo "Usage: $0" 
    echo "     -s <output stl-file>"
    echo "     -j <output obj-file>"
    echo "     -e <executable stl-writer> [-a <scale ratio> (0..1] ]	"
    echo "     -o <osm-file> -m <manhattan-grid: XxY,Dim-xxDim-y,dist-xxdist-y>"
    echo "     -h <height: T,val (T: F=fixed, R=uniformMinMax)>"
    echo "     -b <bounding-box> [0:None, 1:Floor-only, 2:Full (default=1)]"
    echo "     -c <wall colors numer> (default=25)"
    echo "     -r <rotate on x-axes> [0 or 1] (default=1)"
  	
    echo "     -h:  T,val"
    echo "          F,n(umber)                   (fixed height)"
    echo "          R,n1,n2          (random height between n1 and n2)"
    echo "          D,n1,n2          (random height between n1 and n2 with center distance factor)"
    
    exit 1
}



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# MAIN EXECUTION
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

OSM_FILE=""
STL_FILE=""
OBJ_FILE=""
NS3_FILE=""
STL_EXEC=""
ALPHA="1"
MANHATTAN=""
HEIGHT=""
BB="1"
COLORS="25"
ROTATE="1"

while getopts "o:s:n:e:a:m:h:j:b:c:" o; do
    case "${o}" in
        h)
			#echo "PARAMETER ${o}: ${OPTARG}"
            HEIGHT=${OPTARG}
            ;;
        a)
			#echo "PARAMETER ${o}: ${OPTARG}"
            ALPHA=${OPTARG}
            ;;
        b)
			#echo "PARAMETER ${o}: ${OPTARG}"
            BB=${OPTARG}
            ;;
        c)
			#echo "PARAMETER ${o}: ${OPTARG}"
            COLORS=${OPTARG}
            ;;
        o)
			#echo "PARAMETER ${o}: ${OPTARG}"
            OSM_FILE=${OPTARG}
            ;;
        s)
			#echo "PARAMETER ${o}: ${OPTARG}"
            STL_FILE=${OPTARG}
            ;;
        n)
			#echo "PARAMETER ${o}: ${OPTARG}"
            NS3_FILE=${OPTARG}
            ;;
        e)
			#echo "PARAMETER ${o}: ${OPTARG}"
            STL_EXEC=${OPTARG}
            ;;
        m)
			#echo "PARAMETER ${o}: ${OPTARG}"
            MANHATTAN=${OPTARG}
            ;;
        r)
			#echo "PARAMETER ${o}: ${OPTARG}"
            ROTATE=${OPTARG}
            ;;
        j)
			#echo "PARAMETER ${o}: ${OPTARG}"
            OBJ_FILE=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

if [ -z "${STL_FILE}" ] || [ -z "${NS3_FILE}" ] || [ -z "${STL_EXEC}" ] || [ -z "${HEIGHT}" ] || [ -z "${OBJ_FILE}" ]; then    
    usage
fi

if [ -z "${MANHATTAN}" ] && [ -z "${OSM_FILE}" ]; then    
    usage
fi

if [ -n "${MANHATTAN}" ] && [ -n "${OSM_FILE}" ]; then    
	echo "Only one parameter among '-m' and '-o' can be defined"
    usage
fi

if [[ ! ${HEIGHT} =~ ^F,[0-9]+$ ]]; then
	if [[ ! ${HEIGHT} =~ ^R,[0-9]+,[0-9]+$ ]]; then
		if [[ ! ${HEIGHT} =~ ^D,[0-9]+,[0-9]+$ ]]; then
			echo "Please insert a correct parameter -h"
			usage
		fi
	fi
fi

if [[ ! ${BB} =~ ^[0-2]$ ]]; then
		echo "Please insert a correct parameter -b"
		usage
fi

if [[ ! ${COLORS} =~ ^[0-9]+$ ]]; then
		echo "Please insert a correct parameter -c"
		usage
fi


if [ -n "${OSM_FILE}" ]; then  

	echo "Starting osmfilter"
	OSM_FILE_FILTERED=${OSM_FILE}.filtered
	if [ ! -e ${OSM_FILE_FILTERED} ]
	then
		echo "Osmfilter"
		osmfilter ${OSM_FILE} --keep="building" -o=${OSM_FILE_FILTERED}
	else
		echo "Using filtered osm file: ${OSM_FILE_FILTERED}"
	fi

	echo "Starting polyconvert"
	OSM_FILE_POLY=${OSM_FILE}.poly.xml
	if [ ! -e ${OSM_FILE_POLY} ]
	then
		echo "Polyconvert"
		polyconvert --osm-files ${OSM_FILE_FILTERED} -o ${OSM_FILE_POLY}
	else
		echo "Using polygons contained in ${OSM_FILE_POLY}"
	fi

	echo -n "" > $NS3_FILE
	NPOLY=`cat ${OSM_FILE_POLY} | grep "shape=" | wc -l`

	echo "Isolating shapes"
	OSM_FILE_SHAPES=${OSM_FILE}.shapes
	cat ${OSM_FILE_POLY} | grep "shape=" | awk -F"shape=\"" '{print $2}' | awk -F"\"/>" '{print $1}' > ${OSM_FILE_SHAPES}

	echo "Creating STL, OBJ-MTL and NS3 files"
	${STL_EXEC} ${OSM_FILE_SHAPES} ${NS3_FILE} ${STL_FILE} ${ALPHA} void ${HEIGHT} ${OBJ_FILE} ${BB} ${COLORS} ${ROTATE}

	echo "removing temporary files"
	rm -rf ${OSM_FILE_FILTERED}
	rm -rf ${OSM_FILE_POLY}
	rm -rf ${OSM_FILE_SHAPES}

else	
	#XxY;Dim;dist
	if [[ ! $MANHATTAN =~ ^[0-9]+x[0-9]+,[0-9]+x[0-9]+,[0-9]+x[0-9]+,[0-9.]+$ ]]; then
		echo "Please insert a correct parameter -m"
		usage
	fi
	echo "Creating STL and NS3 files"
	${STL_EXEC} void ${NS3_FILE} ${STL_FILE} ${ALPHA} ${MANHATTAN} ${HEIGHT} ${OBJ_FILE} ${BB} ${COLORS} ${ROTATE}
fi











