//  created:    2015/11/05
//  filename:   SimpleFleet.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo fleet
//
//
/*********************************************************************/

#include "CuscusBasic.h"
#include "../../../../../flair-dev/include/FlairCore/Vector3D.h"
#include "../../../../../flair-dev/include/FlairCore/Vector2D.h"
#include <TargetController.h>
#include <Uav.h>
#include <GridLayout.h>
#include <PushButton.h>
#include <DataPlot1D.h>
#include <Ahrs.h>
#include <MetaUsRangeFinder.h>
#include <MetaDualShock3.h>
#include <FrameworkManager.h>
#include <VrpnClient.h>
#include <MetaVrpnObject.h>
#include <TrajectoryGenerator2DCircle.h>
#include <Vector3D.h>
#include <Vector2D.h>
#include <PidThrust.h>
#include <Euler.h>
#include <cvmatrix.h>
#include <AhrsData.h>
#include <Ahrs.h>
#include <DoubleSpinBox.h>
#include <stdio.h>
#include <cmath>
#include <Tab.h>
#include <Pid.h>
#include <UdpSocket.h>
#include <string.h>

#define PI ((float)3.14159265358979323846)

using namespace std;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::sensor;
using namespace flair::filter;
using namespace flair::meta;


CuscusBasic::CuscusBasic(std::string broadcast,std::string optiaddress,flair::sensor::TargetController *controller,
                         int broadcastMS, float l0, float sstiffness): UavStateMachine(controller), behaviourMode(BehaviourMode_t::Default), vrpnLost(false) {
    Uav* uav=GetUav();
		
	//VrpnClient* vrpnclient=new VrpnClient("vrpn", uav->GetDefaultVrpnAddress(),80);
    VrpnClient* vrpnclient=new VrpnClient("vrpn", optiaddress,80);
	uavVrpn = new MetaVrpnObject(uav->ObjectName());
	getFrameworkManager()->AddDeviceToLog(uavVrpn);
	uav->GetAhrs()->YawPlot()->AddCurve(uavVrpn->State()->Element(2),DataPlot::Green);
	vrpnclient->Start();

    circle=new TrajectoryGenerator2DCircle(vrpnclient->GetLayout()->NewRow(),"circle");
    uavVrpn->xPlot()->AddCurve(circle->Matrix()->Element(0,0),0,0,255);
    uavVrpn->yPlot()->AddCurve(circle->Matrix()->Element(0,1),0,0,255);
    uavVrpn->VxPlot()->AddCurve(circle->Matrix()->Element(1,0),0,0,255);
    uavVrpn->VyPlot()->AddCurve(circle->Matrix()->Element(1,1),0,0,255);

    xCircleCenter=new DoubleSpinBox(vrpnclient->GetLayout()->NewRow(),"x circle center"," m",-5,5,0.1,1,0);
    yCircleCenter=new DoubleSpinBox(vrpnclient->GetLayout()->NewRow(),"y circle center"," m",-5,5,0.1,1,0);
    yDisplacement=new DoubleSpinBox(vrpnclient->GetLayout()->NewRow(),"y displacement"," m",0,2,0.1,1,0);

    //parent->AddDeviceToLog(Uz());

    u_x=new Pid(setupLawTab->At(1,0),"u_x");
    u_x->UseDefaultPlot(graphLawTab->NewRow());
    u_y=new Pid(setupLawTab->At(1,1),"u_y");
    u_y->UseDefaultPlot(graphLawTab->LastRowLastCol());

    message=new UdpSocket(uav,"Message",broadcast,true);

    customReferenceOrientation= new AhrsData(this,"reference");
    uav->GetAhrs()->AddPlot(customReferenceOrientation,DataPlot::Yellow);
    AddDataToControlLawLog(customReferenceOrientation);

    customOrientation=new AhrsData(this,"orientation");
/*
    //check init conditions
    Vector3D uav_pos;
    Euler vrpn_euler;
    GetVrpnObject()->GetPosition(uav_pos);
    GetVrpnObject()->GetEuler(vrpn_euler);

    if(name=="x8_0") {
        //x8_0 should be on the left, with 0 yaw
        if(uav_pos.y>yCircleCenter->Value() || vrpn_euler.yaw>20 || vrpn_euler.yaw<-20) Thread::Err("wrong init position\n");
    }
    if(name=="x8_1") {
        //x8_1 should be on the right, with 180 yaw
        if(uav_pos.y<yCircleCenter->Value() || (vrpn_euler.yaw<160 && vrpn_euler.yaw>-160)) Thread::Err("wrong init position %f %f\n",yCircleCenter->Value(),vrpn_euler.yaw);
    }
    */

    sscanf(GetUav()->ObjectName().c_str(), "x%*[48]_%d", &myIdx);

    distanceL0 = l0;
    distanceL0SNR = l0;
    staticNodeSpringStiffness = sstiffness;
    alphaFilterSNR = 0.9; // TODO

    srand(GetTime());

    broadcastTimer = 1000000 * broadcastMS;
    callbackTimer_next=GetTime() + broadcastTimer + ((rand() % 1000) * 1000000);
    char shmname [32];
    snprintf(shmname, sizeof(shmname), "uavSNR_%d", myIdx);
    shmemSNR = new SharedMem(this, shmname, sizeof(shmem_var_t) * MAXUAVNUMBER);

    timeSecUpdateNeigh = 3;

    fprintf(stdout, "Opening shared memory for SNR with name; %s\n", shmname);
}

CuscusBasic::~CuscusBasic() {
}

const AhrsData *CuscusBasic::GetOrientation(void) const {
    //get yaw from vrpn
		Quaternion vrpnQuaternion;
    uavVrpn->GetQuaternion(vrpnQuaternion);

    //get roll, pitch and w from imu
    Quaternion ahrsQuaternion;
    Vector3Df ahrsAngularSpeed;
    GetDefaultOrientation()->GetQuaternionAndAngularRates(ahrsQuaternion, ahrsAngularSpeed);

    Euler ahrsEuler=ahrsQuaternion.ToEuler();
    ahrsEuler.yaw=vrpnQuaternion.ToEuler().yaw;
    Quaternion mixQuaternion=ahrsEuler.ToQuaternion();

    customOrientation->SetQuaternionAndAngularRates(mixQuaternion,ahrsAngularSpeed);

    return customOrientation;
}

void CuscusBasic::AltitudeValues(float &z,float &dz) const {
    Vector3Df uav_pos,uav_vel;

    uavVrpn->GetPosition(uav_pos);
    uavVrpn->GetSpeed(uav_vel);
    //z and dz must be in uav's frame
    z=-uav_pos.z;
    dz=-uav_vel.z;
}

const AhrsData *CuscusBasic::GetReferenceOrientation(void) {
    Vector2Df pos_err, vel_err; // in uav coordinate system
    float yaw_ref;
    Euler refAngles;
    std::string key ("_0");

    std::size_t found = GetUav()->ObjectName().rfind(key);
    if (found!=std::string::npos) {
        PositionValues(pos_err, vel_err, yaw_ref);
    }
    else {
        PositionValues_VirtualForces(pos_err, vel_err, yaw_ref);
    }
    //PositionValues(pos_err, vel_err, yaw_ref);

    refAngles.yaw=yaw_ref;

    u_x->SetValues(pos_err.x, vel_err.x);
    u_x->Update(GetTime());
    refAngles.pitch=u_x->Output();

    u_y->SetValues(pos_err.y, vel_err.y);
    u_y->Update(GetTime());
    refAngles.roll=-u_y->Output();

    customReferenceOrientation->SetQuaternionAndAngularRates(refAngles.ToQuaternion(),Vector3Df(0,0,0));

    return customReferenceOrientation;
}

void CuscusBasic::PositionValues(Vector2Df &pos_error,Vector2Df &vel_error,float &yaw_ref) {
    Vector3Df uav_pos,uav_vel; // in VRPN coordinate system
    Vector2Df uav_2Dpos,uav_2Dvel; // in VRPN coordinate system

    uavVrpn->GetPosition(uav_pos);
    uavVrpn->GetSpeed(uav_vel);

    uav_pos.To2Dxy(uav_2Dpos);
    uav_vel.To2Dxy(uav_2Dvel);

    if (behaviourMode==BehaviourMode_t::PositionHold1 || behaviourMode==BehaviourMode_t::PositionHold2
        || behaviourMode==BehaviourMode_t::PositionHold3 || behaviourMode==BehaviourMode_t::PositionHold4) {
        pos_error=uav_2Dpos-posHold;
        vel_error=uav_2Dvel;
        yaw_ref=yawHold;
    } else { //Circle
        Vector2Df circle_pos,circle_vel;
        Vector2Df target_2Dpos;

        //circle center
        target_2Dpos.x=xCircleCenter->Value();
        target_2Dpos.y=yCircleCenter->Value();
        circle->SetCenter(target_2Dpos);

        //circle reference
        circle->Update(GetTime());
        circle->GetPosition(circle_pos);
        circle->GetSpeed(circle_vel);

        //error in optitrack frame
        pos_error=uav_2Dpos-circle_pos;
        vel_error=uav_2Dvel-circle_vel;
        yaw_ref=PI/2+atan2(uav_pos.y-target_2Dpos.y,uav_pos.x-target_2Dpos.x);
    }
    //error in uav frame
    Quaternion currentQuaternion=GetCurrentQuaternion();
    Euler currentAngles;//in vrpn frame
    currentQuaternion.ToEuler(currentAngles);
    pos_error.Rotate(-currentAngles.yaw);
    vel_error.Rotate(-currentAngles.yaw);
}

void CuscusBasic::SignalEvent(Event_t event) {
    UavStateMachine::SignalEvent(event);

    switch(event) {
    case Event_t::EmergencyStop:
        message->SendMessage("EmergencyStop");
        break;
    case Event_t::TakingOff:
        //behaviourMode=BehaviourMode_t::Default;
        message->SendMessage("TakeOff");
        VrpnPositionHold();
        behaviourMode=BehaviourMode_t::PositionHold1;
        break;
    case Event_t::StartLanding:
        VrpnPositionHold();
        behaviourMode=BehaviourMode_t::PositionHold4;
        message->SendMessage("Landing");
        break;
    case Event_t::EnteringControlLoop:
        CheckMessages();
        if ((behaviourMode==BehaviourMode_t::Circle1) && (!circle->IsRunning())) {
            VrpnPositionHold();
            behaviourMode=BehaviourMode_t::PositionHold2;
            if(posHold.y<0) {
                posHold.y-=yDisplacement->Value();
            } else {
                posHold.y+=yDisplacement->Value();
            }
            posWait=GetTime();
            Printf("Circle1 -> PositionHold2\n");
        }
        if (behaviourMode==BehaviourMode_t::PositionHold2 && GetTime()>(posWait+3*(Time)1000000000)) {
            behaviourMode=BehaviourMode_t::PositionHold3;
            if(posHold.y<0) {
                posHold.y+=yDisplacement->Value();
            } else {
                posHold.y-=yDisplacement->Value();
            }
            posWait=GetTime();
            Printf("PositionHold2 -> PositionHold3\n");
        }
        if (behaviourMode==BehaviourMode_t::PositionHold3 && GetTime()>(posWait+3*(Time)1000000000)) {
            behaviourMode=BehaviourMode_t::Circle2;
            StartCircle();
            Printf("PositionHold3 -> Circle2\n");
        }
        if ((behaviourMode==BehaviourMode_t::Circle2) && (!circle->IsRunning())) {
            Printf("Circle2 -> Land\n");
            behaviourMode=BehaviourMode_t::PositionHold4;
            Land();
        }

        inLoopCommands();

        break;
    case Event_t::EnteringFailSafeMode:
        behaviourMode=BehaviourMode_t::Default;
        break;
    case Event_t::ZTrajectoryFinished:
        Printf("PositionHold1 -> Circle1\n");
        StartCircle();
        behaviourMode=BehaviourMode_t::Circle1;
        break;
    }
}

void CuscusBasic::CheckMessages(void) {
    char msg[64];
    char src[64];
    size_t src_size=sizeof(src);
    while(message->RecvMessage(msg,sizeof(msg),TIME_NONBLOCK,src,&src_size)>0) {
        printf("Received from %s %s - ",GetUav()->ObjectName().c_str(),src);
        printf("%s\n",msg);
        if(strcmp(src,GetUav()->ObjectName().c_str())!=0) {
            /*
            if(strcmp(msg,"StopMotors")==0 && orientation_state!=OrientationState_t::Stopped)
            {
                joy->FlashLed(DualShock3::led1,10,10);
                joy->Rumble(0x70);
                GetBldc()->SetEnabled(false);
                GetUavMultiplex()->UnlockUserInterface();
                altitude_state=AltitudeState_t::Stopped;
                orientation_state=OrientationState_t::Stopped;
                GetAhrs()->UnlockUserInterface();
            }
*/
            if(strcmp(msg,"TakeOff")==0) {
                Printf("TakeOff fleet\n");
                TakeOff();
            }
            if(strcmp(msg,"Landing")==0) {
                Printf("Landing fleet\n");
                Land();
            }
            if(strcmp(msg,"EmergencyStop")==0) {
                Printf("EmergencyStop fleet\n");
                EmergencyStop();
            }
            if(strncmp(msg,"Position",8)==0) {
                int droneIdx;
                float droneX, droneY, droneZ;
                float actSNR, actSignalPow;
                Printf("Position received\n");
                fprintf(stderr, "*****\n%s\n++++++++\n%s\n*****\n", src, msg);fflush(stderr);
                sscanf(src, "x%*[48]_%d", &droneIdx);
                sscanf(msg, "Position [%f;%f;%f]", &droneX, &droneY, &droneZ);

                readSNRshmem(actSNR, actSignalPow, droneIdx, myIdx);

                if (neighborhood.count(droneIdx) == 0) {
                    neighborhood[droneIdx].filteredSNR = actSNR;
                }
                else {
                    float oldSNR = neighborhood[droneIdx].filteredSNR;
                    neighborhood[droneIdx].filteredSNR = oldSNR + alphaFilterSNR * (actSNR - oldSNR);
                }

                neighborhood[droneIdx].snr.snrDB = actSNR;
                neighborhood[droneIdx].snr.signalPowDB = actSignalPow;
                neighborhood[droneIdx].pos = Vector3Df(droneX, droneY, droneZ);
                neighborhood[droneIdx].addr = droneIdx;
                neighborhood[droneIdx].timestamp = GetTime();

                fprintf(stderr, "Red -> IDX: %d; POS:[%f;%f;%f]\n", droneIdx, droneX, droneY, droneZ);fflush(stderr);
                fprintf(stderr, "SNR %d [%f ---> %f]\n", droneIdx, actSNR, neighborhood[droneIdx].filteredSNR);fflush(stderr);

                //neighborhood[droneIdx] = Vector3Df(droneX, droneY, droneZ);

            }
        }
    }
}

void CuscusBasic::ExtraSecurityCheck(void) {
    if (!vrpnLost && behaviourMode!=BehaviourMode_t::Default) {
        if (!uavVrpn->IsTracked(500)) {
            Thread::Err("Optitrack, uav lost\n");
            vrpnLost=true;
            EnterFailSafeMode();
            Land();
        }
    }
}

void CuscusBasic::ExtraCheckJoystick(void) {

}

void CuscusBasic::StartCircle(void) {
    if (SetOrientationMode(OrientationMode_t::Custom)) {
        Thread::Info("Demo flotte: start circle\n");
    } else {
        Thread::Warn("Demo flotte: could not start circle\n");
        return;
    }
    Vector3Df uav_pos;
    Vector2Df uav_2Dpos,target_2Dpos;

    //circle center
    target_2Dpos.x=xCircleCenter->Value();
    target_2Dpos.y=yCircleCenter->Value();
    circle->SetCenter(target_2Dpos);

    uavVrpn->GetPosition(uav_pos);
    uav_pos.To2Dxy(uav_2Dpos);
    circle->StartTraj(uav_2Dpos,1);

    u_x->Reset();
    u_y->Reset();
}

void CuscusBasic::StopCircle(void) {
    circle->FinishTraj();
    //joy->Rumble(0x70);
    Thread::Info("Demo flotte: finishing circle\n");
}

void CuscusBasic::VrpnPositionHold(void) {
  if (SetOrientationMode(OrientationMode_t::Custom)) {
        Thread::Info("Demo flotte: holding position\n");
    } else {
        Thread::Info("Demo flotte: could not hold position\n");
        //return;
    }  
		
		Quaternion vrpnQuaternion;
    uavVrpn->GetQuaternion(vrpnQuaternion);
		yawHold=vrpnQuaternion.ToEuler().yaw;

		Vector3Df vrpnPosition;
    uavVrpn->GetPosition(vrpnPosition);
    vrpnPosition.To2Dxy(posHold);

    u_x->Reset();
    u_y->Reset();
}


void CuscusBasic::getNeighborhoodCenterPoint(flair::core::Vector2Df *p) {
    //if ((true) || (neighborhood.empty())) {
        p->x=xCircleCenter->Value();
        p->y=yCircleCenter->Value();
    /*}
    else {
        Vector3D<double> uav_pos;
        Vector2D<double> centerSum;

        GetUav()->GetVrpnObject()->GetPosition(uav_pos);
        uav_pos.To2Dxy(centerSum);

        for (std::map<int,flair::core::Vector3D<double>>::iterator it = neighborhood.begin(); it != neighborhood.end(); it++) {
            Vector2D<double> pos2dval;

            it->second.To2Dxy(pos2dval);

            centerSum = centerSum + pos2dval;
        }

        centerSum = centerSum / (((float) neighborhood.size()) + 1.0);
        p->x=centerSum.x;
        p->y=centerSum.y;
    }*/
}

void CuscusBasic::readSNRshmem(float &snr, float &sPower, int rcvIdx, int myIdx) {
    shmem_var_t buffSHMEM[MAXUAVNUMBER];

    shmemSNR->Read((char *)buffSHMEM, sizeof(shmem_var_t) * MAXUAVNUMBER);

    snr = buffSHMEM[rcvIdx].snrDB;
    sPower = buffSHMEM[rcvIdx].signalPowDB;

    //fprintf(stdout, "Reading from shared SNR memory of UAV_%d. SNR from UAV_%d is %f\n", myIdx, rcvIdx, snr);
}

void CuscusBasic::sendBroadcastPosition(void) {

    char msgPos[64];
    Vector3Df uav_pos;

    uavVrpn->GetPosition(uav_pos);

    snprintf(msgPos, sizeof(msgPos), "Position [%lf;%lf;%lf]", uav_pos.x, uav_pos.y, uav_pos.z);

    message->SendMessage(msgPos);

    //DEBUG
    fprintf(stderr, "Sending -> %s\n", msgPos);fflush(stderr);
    for (auto it = neighborhood.begin(); it != neighborhood.end(); it++) {
    	fprintf(stderr, "IDX: %d; POS:[%f;%f;%f]\n", it->first, it->second.pos.x, it->second.pos.y, it->second.pos.z);fflush(stderr);
    }
}

void CuscusBasic::PositionValues_VirtualForces(Vector2Df &pos_error,Vector2Df &vel_error,float &yaw_ref) {
    Vector3Df uav_pos,uav_vel; // in VRPN coordinate system
    Vector2Df uav_2Dpos,uav_2Dvel; // in VRPN coordinate system
    //Euler vrpn_euler; // in VRPN coordinate system

    uavVrpn->GetPosition(uav_pos);
    uavVrpn->GetSpeed(uav_vel);
    //uavVrpn->GetEuler(vrpn_euler);

    uav_pos.To2Dxy(uav_2Dpos);
    uav_vel.To2Dxy(uav_2Dvel);

    if (behaviourMode==BehaviourMode_t::PositionHold1 || behaviourMode==BehaviourMode_t::PositionHold2
        || behaviourMode==BehaviourMode_t::PositionHold3 || behaviourMode==BehaviourMode_t::PositionHold4) {
        pos_error=uav_2Dpos-posHold;
        vel_error=uav_2Dvel;
        yaw_ref=yawHold;
    } else { //Virtual Spring mesh
        Vector2Df mesh_pos,mesh_vel;
        //Vector2Df force = getForceFilterNodeListAcuteAngleTest();
        Vector2Df force = getForceFilterNodeListAcuteAngleTestSNR();

        mesh_pos = uav_2Dpos + force;
        mesh_vel = force / 10.0; //TODO


        //error in optitrack frame
        pos_error=uav_2Dpos-mesh_pos;
        vel_error=uav_2Dvel-mesh_vel;
        //yaw_ref=PI/2+atan2(uav_pos.y-target_2Dpos.y,uav_pos.x-target_2Dpos.x);
        yaw_ref=yawHold;
    }
    //error in uav frame
    Quaternion currentQuaternion=GetCurrentQuaternion();
    Euler currentAngles;//in vrpn frame
    currentQuaternion.ToEuler(currentAngles);
    pos_error.Rotate(-currentAngles.yaw);
    vel_error.Rotate(-currentAngles.yaw);
}


flair::core::Vector2Df CuscusBasic::getForceFilterNodeListAcuteAngleTest(void) {
    //Coord myPos = getCurrentPosition();
    std::map<int,flair::core::Vector3Df> filtered;
    Vector3Df uav_pos;
    Vector2Df uav_2Dpos;
    Vector2Df ris;
    ris.x = 0;
    ris.y = 0;

    uavVrpn->GetPosition(uav_pos);
    uav_pos.To2Dxy(uav_2Dpos);

    // filter the neighbours that pass the "acute angle" test
    filtered.clear();
    for (auto it = neighborhood.begin(); it != neighborhood.end(); it++) {
        bool acute_angle_result = true;
        Vector2Df it_2Dpos;
        Vector3Df *itPos = &(it->second.pos);
        itPos->To2Dxy(it_2Dpos);

        for (auto check = neighborhood.begin(); check != neighborhood.end(); check++) {
            if (check != it) {
                Vector2Df check_2Dpos;
                Vector3Df *checkPos = &(check->second.pos);
                checkPos->To2Dxy(check_2Dpos);

                /****************************************/
                double angle = calculateAngle( uav_2Dpos, check_2Dpos, it_2Dpos );

                if (angle == 0) {
                    if (	(calcDistance2D(uav_2Dpos, check_2Dpos) < calcDistance2D(uav_2Dpos, it_2Dpos)) &&
                            (calcDistance2D(it_2Dpos, check_2Dpos) < calcDistance2D(it_2Dpos, uav_2Dpos))	) {
                        acute_angle_result = false;
                        break;
                    }
                }
                else if((angle > (M_PI / 2)) || (angle < (-(M_PI / 2)))) {
                    acute_angle_result = false;
                    break;
                }
                /****************************************/
            }
        }

        if (acute_angle_result) {
            filtered[it->first]=it->second.pos;
        }
    }

    // for each neghbour, add a new attractive or repulsice force (depending on the relative distance)
    for (auto it = filtered.begin(); it != filtered.end(); it++) {
        Vector2Df it_2Dpos, unity, stableP, newForce;
        Vector3Df *itPos = &(it->second);
        itPos->To2Dxy(it_2Dpos);

        double dist = calcDistance2D(uav_2Dpos, it_2Dpos);

        unity = it_2Dpos - uav_2Dpos;
        unity.Normalize();

        stableP = uav_2Dpos + (unity * distanceL0);

        newForce = it_2Dpos - stableP;

        ris = ris + newForce;

    }

    //fprintf(stderr, "Resulting force BEFORE: [%f:%f]\n", ris.x, ris.y); fflush(stderr);

    // don't move if the force is small
    if (ris.GetNorm() <= 0.02) {
        ris.x = 0;
        ris.y = 0;
    }

    //fprintf(stderr, "Resulting force  AFTER: [%f:%f]\n\n", ris.x, ris.y); fflush(stderr);

    return ris;
}


float CuscusBasic::calcSpringLengthFromLinkBudget(float referenceLinkBudget, float actualLinkBudget) {
    float ris = 0;
    float alphaPathLoss = 2.0;

    if(actualLinkBudget >= referenceLinkBudget) {
        // la forza deve essere di repulsione, quindi ritorno un valore negativo
        //ris = -(sqrt(actualLinkBudget / referenceLinkBudget) - 1);
        ris = -(pow( ((actualLinkBudget / referenceLinkBudget) - 1), 1/alphaPathLoss));
    }
    else if(actualLinkBudget > 0){
        // la forza deve essere di attrazione, quindi ritorno un valore positivo
        //ris = sqrt(referenceLinkBudget / actualLinkBudget) - 1;
        ris = pow( ((referenceLinkBudget / actualLinkBudget) - 1), 1/alphaPathLoss);
    }

    return ris;
}

void CuscusBasic::checkOldNeighbourood(void) {
    bool toRemove = true;
    while (toRemove) {
        toRemove = false;
        for (auto it = neighborhood.begin(); it != neighborhood.end(); it++) {
            if ((GetTime() - (it->second).timestamp) > (timeSecUpdateNeigh * 1000000000.0)) {
                neighborhood.erase(it);
                toRemove = true;
                break;
            }
        }
    }
}

flair::core::Vector2Df CuscusBasic::getForceFilterNodeListAcuteAngleTestSNR(void) {
    //Coord myPos = getCurrentPosition();
    std::map<int,drone_info_t> filtered;
    Vector3Df uav_pos;
    Vector2Df uav_2Dpos;
    Vector2Df ris;
    ris.x = 0;
    ris.y = 0;

    uavVrpn->GetPosition(uav_pos);
    uav_pos.To2Dxy(uav_2Dpos);

    checkOldNeighbourood();

    // filter the neighbours that pass the "acute angle" test
    filtered.clear();
    for (auto it = neighborhood.begin(); it != neighborhood.end(); it++) {
        bool acute_angle_result = true;
        Vector2Df it_2Dpos;
        Vector3Df *itPos = &((it->second).pos);
        itPos->To2Dxy(it_2Dpos);

        for (auto check = neighborhood.begin(); check != neighborhood.end(); check++) {
            if (check != it) {
                Vector2Df check_2Dpos;
                Vector3Df *checkPos = &((check->second).pos);
                checkPos->To2Dxy(check_2Dpos);

                /****************************************/
                double angle = calculateAngle( uav_2Dpos, check_2Dpos, it_2Dpos );

                if (angle == 0) {
                    if (	(calcDistance2D(uav_2Dpos, check_2Dpos) < calcDistance2D(uav_2Dpos, it_2Dpos)) &&
                            (calcDistance2D(it_2Dpos, check_2Dpos) < calcDistance2D(it_2Dpos, uav_2Dpos))	) {
                        acute_angle_result = false;
                        break;
                    }
                }
                else if((angle > (M_PI / 2)) || (angle < (-(M_PI / 2)))) {
                    acute_angle_result = false;
                    break;
                }
                /****************************************/
            }
        }

        if (acute_angle_result) {
            filtered[it->first]=it->second;
        }
    }

    // for each neghbour, add a new attractive or repulsice force (depending on the relative distance)
    for (auto it = filtered.begin(); it != filtered.end(); it++) {
        float dx, dy, r, theta, diffSpring, forceX, forceY;
        Vector2Df it_2Dpos;
        Vector3Df *itPos = &((it->second).pos);
        itPos->To2Dxy(it_2Dpos);

        //calculate the distance vector
        dx = it_2Dpos.x - uav_2Dpos.x;
        dy = it_2Dpos.y - uav_2Dpos.y;

        UsefulTools::cartesian2polar(dx, dy, r, theta);

        //diffSpring = calcSpringLengthFromLinkBudget(distanceL0SNR, it->second.snr.snrDB);
        diffSpring = calcSpringLengthFromLinkBudget(distanceL0SNR, it->second.filteredSNR);

        if (diffSpring < 0) {    // repulsion force
            theta = (theta > 0) ? (theta - PI) : (theta + PI);   // reverse the angle
        }

        UsefulTools::polar2cartesian(staticNodeSpringStiffness * fabs(diffSpring), theta, forceX, forceY);    //calcolo il vettore della forza

        ris = ris + Vector2Df(forceX, forceY);

        /*Vector2D it_2Dpos, unity, stableP, newForce;
        Vector3D *itPos = &((it->second).pos);
        itPos->To2Dxy(it_2Dpos);

        double dist = calcDistance2D(uav_2Dpos, it_2Dpos);

        unity = it_2Dpos - uav_2Dpos;
        unity.Normalize();

        stableP = uav_2Dpos + (unity * distanceL0);

        newForce = it_2Dpos - stableP;

        ris = ris + newForce;*/

    }

    //fprintf(stderr, "Resulting force BEFORE: [%f:%f]\n", ris.x, ris.y); fflush(stderr);

    // don't move if the force is small
    if (ris.GetNorm() <= 0.02) {
        ris.x = 0;
        ris.y = 0;
    }

    //fprintf(stderr, "Resulting force  AFTER: [%f:%f]\n\n", ris.x, ris.y); fflush(stderr);

    return ris;
}

double CuscusBasic::calcDistance2D(flair::core::Vector2Df a, flair::core::Vector2Df b) {
    return (sqrt(pow(a.x-b.x, 2.0) + pow(a.y-b.y, 2.0)));
}

double CuscusBasic::calculateAngle(flair::core::Vector2Df a, flair::core::Vector2Df b, flair::core::Vector2Df c) {
    double vABx, vABy, vABz, vCBx, vCBy, vCBz, tmp1,tmp2;

    vABx = a.x - b.x;
    vABy = a.y - b.y;
    //vABz = a.z - b.z;
    vABz = 0;
    vCBx = c.x - b.x;
    vCBy = c.y - b.y;
    //vCBz = c.z - b.z;
    vCBz = 0;

    tmp1 = vABx*vCBx + vABy*vCBy + vABz*vCBz;
    tmp2 = sqrt(((vABx*vABx) + (vABy*vABy) + (vABz*vABz)) * ((vCBx*vCBx) + (vCBy*vCBy) + (vCBz*vCBz)));

    if(tmp2 != 0) {
        return acos(tmp1/tmp2);
    }
    else {
        return 0;
    }
}

void CuscusBasic::inLoopCommands (void) {
    /*float alt, altv;
    float up,upv;
    float left,leftv;
    float right,rightv;
    float front,frontv;
    float rear,rearv;

    FailSafeAltitudeValues(alt, altv);
    FailSafeUSUPValues(up, upv);
    FailSafeUSLEFTValues(left,leftv);
    FailSafeUSRIGHTValues(right,rightv);
    FailSafeUSFRONTValues(front,frontv);
    FailSafeUSREARValues(rear,rearv);

    fprintf(stderr, "Sensore sotto: %f [%f]\n", alt, altv);
    fprintf(stderr, "Sensore sopra: %f [%f]\n", up, upv);
    fprintf(stderr, "Sensore sinistra: %f [%f]\n", left,leftv);
    fprintf(stderr, "Sensore destra: %f [%f]\n", right,rightv);
    fprintf(stderr, "Sensore davanti: %f [%f]\n", front,frontv);
    fprintf(stderr, "Sensore dietro: %f [%f]\n", rear,rearv);


    fprintf(stderr, "\n");
    fflush(stderr);*/
    flair::core::Time currentT = GetTime();
    if (timer1sec <= currentT) {
        //fprintf(stderr, "Making 1sec stats\n");
        make1stat(currentT);
        if ((currentT - timer1sec) > 2000000000) {
            timer1sec = currentT + 1000000000;
        } else {
            timer1sec += 1000000000;
        }
    }

    flair::core::Time now = GetTime();
    if (now > callbackTimer_next) {
        //fprintf(stderr, "ExtraSecurityCheck()\n");fflush(stderr);

        sendBroadcastPosition();

        //callbackTimer_next = GetTime() + broadcastTimer;
        callbackTimer_next = GetTime() + broadcastTimer + ((rand() % 10000000) - 5000000);
    }

    /*{
        char filenameBuff[64];

        snprintf(filenameBuff, sizeof(filenameBuff), "/tmp/input_%s.cmd", GetUav()->ObjectName().c_str());

        //std::string key("_1");
        //std::size_t found = GetUav()->ObjectName().rfind(key);
        //if (found != std::string::npos) {

        FILE *ftmp;
        int off = 3;

        Vector3D uav_pos;
        Vector2D uav_2Dpos, offsetP, nextP; // in VRPN coordinate system
        Euler uav_euler; // in VRPN coordinate system

        GetUav()->GetVrpnObject()->GetPosition(uav_pos);
        GetUav()->GetVrpnObject()->GetEuler(uav_euler);

        uav_pos.To2Dxy(uav_2Dpos);

        //std::string readS;
        //cin >> readS;
        ftmp = fopen(filenameBuff, "r");
        if (ftmp) {
            char bbb[128];
            bool done = false;
            fgets(bbb, sizeof(bbb), ftmp);

            std::string readS = std::string(bbb);
            fprintf(stderr, "Read: %s -> ", readS.c_str());

            if (readS.compare(0, 1, "q") == 0) {
                fprintf(stderr, "OK q\n");
                offsetP = Vector2D(off, -off);
            } else if (readS.compare(0, 1, "w") == 0) {
                fprintf(stderr, "OK w\n");
                offsetP = Vector2D(off, 0);
            } else if (readS.compare(0, 1, "e") == 0) {
                fprintf(stderr, "OK e\n");
                offsetP = Vector2D(off, off);
            } else if (readS.compare(0, 1, "a") == 0) {
                fprintf(stderr, "OK a\n");
                offsetP = Vector2D(0, -off);
            } else if (readS.compare(0, 1, "d") == 0) {
                fprintf(stderr, "OK d\n");
                offsetP = Vector2D(0, off);
            } else if (readS.compare(0, 1, "z") == 0) {
                fprintf(stderr, "OKz\n");
                offsetP = Vector2D(-off, -off);
            } else if (readS.compare(0, 1, "x") == 0) {
                fprintf(stderr, "OK x\n");
                offsetP = Vector2D(-off, 0);
            } else if (readS.compare(0, 1, "c") == 0) {
                fprintf(stderr, "OK c\n");
                offsetP = Vector2D(-off, off);
            } else if (readS.compare(0, 1, "p") == 0) {
                float fx, fy;
                sscanf(readS.c_str(), "p%f;%f\n", &fx, &fy);
                pathList.push_back(Vector2D(fx, fy));
                done = true;
            } else {
                fprintf(stderr, "NONE!!!\n");
                pathList.clear();
                done = true;
            }

            if (!done) {
                offsetP.Rotate(uav_euler.yaw);
                nextP = uav_2Dpos + offsetP;
                pathList.push_back(nextP);
            }

            fclose(ftmp);
            //remove("/tmp/input.cmd");
            remove(filenameBuff);

            //if (pathList.size() > 0) {
            //    bool first = true;
            //    fprintf(stderr, "[%f - %f] -(", uav_2Dpos.x, uav_2Dpos.y);
            //    for (auto it = pathList.begin(); it != pathList.end(); it++) {
            //        if (first) {
            //            fprintf(stderr, "%f)-> [%f - %f] ", calcDistance2D(uav_2Dpos, *it), it->x, it->y);
            //            first = false;
            //        }
            //        else {
            //            fprintf(stderr, "[%f - %f] ", it->x, it->y);
            //        }
            //    }
            //    fprintf(stderr, "\n");
            //}
            //else {
            //    fprintf(stderr, "[%f - %f] ---> []\n", uav_2Dpos.x, uav_2Dpos.y);
            //}
            //fflush(stderr);
        }

        if (pathList.size() > 0) {
            bool first = true;
            fprintf(stderr, "[%f - %f] -(", uav_2Dpos.x, uav_2Dpos.y);
            for (auto it = pathList.begin(); it != pathList.end(); it++) {
                if (first) {
                    fprintf(stderr, "%f)-> [%f - %f] ", calcDistance2D(uav_2Dpos, *it), it->x, it->y);
                    first = false;
                } else {
                    fprintf(stderr, "[%f - %f] ", it->x, it->y);
                }
            }
            fprintf(stderr, "\n");
        } else {
            //fprintf(stderr, "[%f - %f] ---> []\n", uav_2Dpos.x, uav_2Dpos.y);
        }
        fflush(stderr);

        //}
    }*/
}

void CuscusBasic::make1stat(flair::core::Time timeNow) {
    //logConnectivityMatrix(timeNow);

    //logPositionOrientationVariability(timeNow);

    //logCoverageIndex(timeNow);

    //if (myIdx == 0) {
    //    if (firstLog == 0) {
    //        fprintf(stdout, "Time Simulation: 0\n");
    //    }
    //    else {
    //        fprintf(stdout, "Time Simulation: %lu\n", (timeNow - firstLog) / 1000000000);
    //    }
    //}
}
