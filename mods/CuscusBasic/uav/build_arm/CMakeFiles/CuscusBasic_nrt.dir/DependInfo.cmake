# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-src/demos/CuscusBasic/uav/src/CuscusBasic.cpp" "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-src/demos/CuscusBasic/uav/build_arm/CMakeFiles/CuscusBasic_nrt.dir/src/CuscusBasic.cpp.o"
  "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-src/demos/CuscusBasic/uav/src/main.cpp" "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-src/demos/CuscusBasic/uav/build_arm/CMakeFiles/CuscusBasic_nrt.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/opt/robomap3/1.7.3/armv7a-neon/sysroots/armv7a-vfp-neon-poky-linux-gnueabi/usr/include/libxml2"
  "/opt/robomap3/1.7.3/armv7a-neon/sysroots/armv7a-vfp-neon-poky-linux-gnueabi/usr/include/opencv"
  "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-dev/include/FlairCore"
  "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-dev/include/FlairVisionFilter"
  "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-dev/include/FlairSensorActuator"
  "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-dev/include/FlairFilter"
  "/media/angelo/BigLinux/Programs/FLAIR_CUSCUS/flair-dev/include/FlairMeta"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
