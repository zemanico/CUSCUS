#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QProcess>
#include <QDebug>
#include <QDialog>
#include <QMenu>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QTextBrowser>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMenuBar>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    void createMenu();
    void createExecutionButtons();
    void createTextSpace();

    QPushButton *m_button;
    QPushButton *m_execute_script_button;

    QMenuBar *m_menuBar;
    QTextEdit *m_bigEditor;
    QGroupBox *m_executionButtons;

    QMenu *m_fileMenu;
    QAction *m_exitAction;

    QProcess *m_theProcess;

    QString m_scriptLocation;
    QString m_workingDirectory;

private slots:
    void slotExecuteScript(bool executedOnce);
    void slotStdOutAvailable();
    void slotFinishedExecution(int,QProcess::ExitStatus);


};

#endif // WIDGET_H
