/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <unistd.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <ifaddrs.h>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/tap-bridge-module.h"

#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/wifi-phy-standard.h"
#include "ns3/trace-helper.h"
#include "ns3/wifi-mac-helper.h"
#include "ns3/wifi-phy.h"

#include <ns3/spectrum-helper.h>
#include <ns3/buildings-helper.h>
#include <ns3/hybrid-buildings-propagation-loss-model.h>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("UavConnectedSimBuilding");

#define MAXUAVNUMBER 100

typedef struct shmem_var {
    float snrDB;
    float signalPowDB;
} shmem_var_t;

int fds[MAXUAVNUMBER];
sem_t *sems[MAXUAVNUMBER];
shmem_var_t *mem_segments[MAXUAVNUMBER];

void initSharedMemorySNR(int numNodes) {

    //std::string sem_name, shm_name;
    char sem_name[64];
    char shm_name[64];
    int sizeSNRshmem = sizeof(shmem_var_t);

    for (int i = 0; i < numNodes; i++) {    
        //shm_name = "/uavSNR_" + i;
        //sem_name = "/sem_uavSNR_" + i;
        sprintf(shm_name, "/uavSNR_%d", i);
        sprintf(sem_name, "/uavSNR_%d", i);
        
        fds[i] = shm_open(shm_name, O_RDWR | O_CREAT, 0666);
        if (fds[i] == -1) {
            perror("Error creating shared memory");
        }
        ftruncate(fds[i], sizeSNRshmem * MAXUAVNUMBER);

        sems[i] = sem_open(sem_name, O_CREAT, 0666, 1);
        if (sems[i] == SEM_FAILED) {
            perror("Error creating semaphore");
        }

        /*for (int j = 0; j < numNodes; j++) { 
            mem_segments[i][j] = (shmem_var_t *)mmap(NULL, sizeSNRshmem, PROT_READ | PROT_WRITE, MAP_SHARED, fds[i], 0);
            if (mem_segments[i][j] == MAP_FAILED) {
                perror("Failed to map memory");
            }
        }*/
        mem_segments[i] = (shmem_var_t *)mmap(NULL, sizeSNRshmem * MAXUAVNUMBER, PROT_READ | PROT_WRITE, MAP_SHARED, fds[i], 0);
        if (mem_segments[i] == MAP_FAILED) {
            perror("Failed to map memory");
        }
    }
}

void delSharedMemorySNR(int numNodes) {
    int status;
    //std::string sem_name, shm_name;
    char sem_name[64];
    char shm_name[64];
    int sizeSNRshmem = sizeof(shmem_var_t);

    for (int i = 0; i < numNodes; i++) {     
        //shm_name = "/uavSNR_" + i;
        //sem_name = "/sem_uavSNR_" + i;
        sprintf(shm_name, "/uavSNR_%d", i);
        sprintf(sem_name, "/uavSNR_%d", i);
        
        /*for (int j = 0; j < numNodes; j++) { 
            status = munmap(mem_segments[i][j], sizeSNRshmem);
            if (status != 0) {
                fprintf(stderr, "Failed to unmap memory (%s)", strerror(-status));
            }
        }*/
        status = munmap(mem_segments[i], sizeSNRshmem * MAXUAVNUMBER);
        if (status != 0) {
            fprintf(stderr, "Failed to unmap memory (%s)", strerror(-status));
        }

        status = close(fds[i]);
        if (status != 0) {
            fprintf(stderr, "Failed to close file (%s)", strerror(-status));
        }

        // do not check erros as it can be done by another process
        status = shm_unlink(shm_name);
        /* if(status!=0) {
            self->Err("Failed to unlink memory (%s)\n",strerror(-status));
        } */
        
        // do not check erros as it can be done by another process
        status = sem_unlink(sem_name); 
        /* if(status!=0) {
            self->Err("Failed to unlink semaphore (%s)\n",strerror(-status));
        } */

        status = sem_close(sems[i]);
        if (status != 0) {
            fprintf(stderr, "Failed to close semaphore (%s)\n", strerror(-status));
        }
    }
}

void writeSharedMemorySNR(int idxNodes, float snr, float signal, int idxNodeSNR) {
    shmem_var_t newVal;
    
    newVal.snrDB = snr;
    newVal.signalPowDB = signal;
    
    sem_wait(sems[idxNodes]);
    memcpy(&mem_segments[idxNodes][idxNodeSNR], &newVal, sizeof(shmem_var_t));
    sem_post(sems[idxNodes]);
}


/*static void
CourseChange (std::string foo, Ptr<const MobilityModel> mobility)
{
  if ((rand()%100) == 0 )
  {
    Vector pos = mobility->GetPosition ();
    Vector vel = mobility->GetVelocity ();
    std::cout << Simulator::Now () << ", model=" << mobility << ", POS: x=" << pos.x << ", y=" << pos.y
              << ", z=" << pos.z << "; VEL:" << vel.x << ", y=" << vel.y
             << ", z=" << vel.z << std::endl;
  }
}*/



#define BUFFBUFFSIZE 256
static void
//MonitorSnifferRx (Ptr<const Packet> packet, uint16_t channelFreqMhz, uint16_t channelNumber, uint32_t rate, WifiPreamble preamble, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise, int appAddr)
MonitorSnifferRx (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise, int appAddr)
{
    int droneIdx;
    float droneX, droneY, droneZ;
    uint8_t buff[BUFFBUFFSIZE];
    
    memset(buff, 0, BUFFBUFFSIZE);
    packet->CopyData(buff, BUFFBUFFSIZE);
    
    /*uint32_t cpn = packet->CopyData(buff, BUFFBUFFSIZE);
    fprintf(stderr, "Packet received: \"");
    for (unsigned int i = 0; i < cpn; i++) {
		fprintf(stderr, "%c ", buff[i]);
	}
	fprintf(stderr, "\"\n");fflush(stderr);*/
    
    if (strstr((char *)(&buff[60]), ":Position")) {
    
        sscanf((char *)(&buff[60]), "x%*[48]_%d:Position [%f;%f;%f]", &droneIdx, &droneX, &droneY, &droneZ);
        
        //fprintf(stderr, "Red -> IDX: %d; POS:[%f;%f;%f]\n", droneIdx, droneX, droneY, droneZ);fflush(stderr);
        
        //std::cout << "[" << appAddr << "] Received from " << (char *)(&buff[60]) << ". SNR: " << signalNoise.signal - signalNoise.noise << std::endl;
        //std::cout << "[" << appAddr << "] Received from " << droneIdx << " [" << droneX << ";" << droneY << ";" << droneZ << "]. SNR: " << signalNoise.signal - signalNoise.noise << std::endl << std::endl;
        
        writeSharedMemorySNR(appAddr, signalNoise.signal - signalNoise.noise, signalNoise.signal, droneIdx);
    }
}

/*
static void
MonitorSnifferRx (Ptr<const Packet> packet, uint16_t channelFreqMhz, uint16_t channelNumber, uint32_t rate, WifiPreamble preamble, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise, int appAddr)
{
    uint8_t buff[256];
    packet->EnablePrinting();
    std::cout << "APPADDR: " << appAddr << std::endl;
    std::cout << "Packet: " << packet->ToString() << std::endl;
    packet->PrintPacketTags(std::cout);
    packet->Print(std::cout);
    std::cout << std::endl;
    
    memset(buff, 0, 256);
    packet->CopyData(buff, 256);
    
    //for (int i = 0; i < 250; i++) {
    //    std::cout << "IDX: " << i << ": " << (char *)(&buff[i]) << std::endl;
    //}
    std::cout << "IDX: " << 60 << ": " << (char *)(&buff[60]) << std::endl;
    
    uint8_t *buffer = new uint8_t[packet->GetSize()]; 
    packet->CopyData (buffer, packet->GetSize());
    std::string data = std::string((char*)buffer);
    
    std::cout << "SIZEBUFF: " << packet->GetSize() << " - BUFF: " << data << std::endl;
    std::cout << "SNR: " << signalNoise.signal - signalNoise.noise << " (" << signalNoise.signal << "/" << signalNoise.noise << ")" << std::endl;
    
    std::cout << std::endl << std::endl;
}
*/

static void
MonitorSnifferRx0 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 0);
}
static void
MonitorSnifferRx1 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 1);
}
static void
MonitorSnifferRx2 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 2);
}
static void
MonitorSnifferRx3 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 3);
}
static void
MonitorSnifferRx4 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 4);
}
static void
MonitorSnifferRx5 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 5);
}
static void
MonitorSnifferRx6 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 6);
}
static void
MonitorSnifferRx7 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 7);
}
static void
MonitorSnifferRx8 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 8);
}
static void
MonitorSnifferRx9 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 9);
}
static void
MonitorSnifferRx10 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 10);
}
static void
MonitorSnifferRx11 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 11);
}
static void
MonitorSnifferRx12 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 12);
}
static void
MonitorSnifferRx13 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 13);
}
static void
MonitorSnifferRx14 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 14);
}
static void
MonitorSnifferRx15 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 15);
}
static void
MonitorSnifferRx16 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 16);
}
static void
MonitorSnifferRx17 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 17);
}
static void
MonitorSnifferRx18 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 18);
}
static void
MonitorSnifferRx19 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 19);
}
static void
MonitorSnifferRx20 (Ptr<const Packet> packet, uint16_t channelFreqMhz, WifiTxVector txVector, MpduInfo aMpdu, SignalNoiseDbm signalNoise)
{
    MonitorSnifferRx(packet, channelFreqMhz, txVector, aMpdu, signalNoise, 20);
}
/*
static void 
MonitorSniffRx (Ptr<const Packet> packet, uint16_t channelFreqMhz,
                     uint16_t channelNumber, uint32_t rate,
                     WifiPreamble preamble, WifiTxVector txVector,
                     MpduInfo aMpdu, SignalNoiseDbm signalNoise)

{
  g_samples++;
  g_signalDbmAvg += ((signalNoise.signal - g_signalDbmAvg) / g_samples);
  g_noiseDbmAvg += ((signalNoise.noise - g_noiseDbmAvg) / g_samples);
  g_rate = rate;
  g_channelNumber = channelNumber;
}
*/

int
main (int argc, char *argv[])
{
    FILE *fbuilding;
	uint32_t numberUAVs = 3;
	double simTime = 60.;
	std::string tapBaseName = std::string("tapdev");
	//std::string ipBase = std::string("10.1.1.0");
	std::string shmemBaseName = std::string("uav_");
	std::string shmemStruct = std::string("quaternion");
	//std::string delayFN = std::string("/tmp/delay");
	std::string buildingsFile = std::string("");

	CommandLine cmd;
	cmd.AddValue ("simTime", "Simulation time (seconds)", simTime);
	cmd.AddValue ("nUav", "Number of UAVs to simulate", numberUAVs);
	cmd.AddValue ("tapBaseName", "Base name of the OS tap devices (0..nUav-1)", tapBaseName);
	cmd.AddValue ("shmemBaseName", "Base name of the shared memory (0..nUav-1)", shmemBaseName);
	//cmd.AddValue ("delayLogFileName", "File name for delay log file", delayFN);
	cmd.AddValue ("shmemStruct", "Shared memory structure [euler|quaternion]", shmemStruct);
	//cmd.AddValue ("ipBase", "IP sub-net (255.255.255.0) for the net device (X.X.X.1 .. X.X.X.nUav)", ipBase);
	cmd.AddValue ("buildingFileName", "File path of the file .ns3 containing the buildings informations", buildingsFile);
	cmd.Parse (argc, argv);

	//
	// We are interacting with the outside, real, world.  This means we have to
	// interact in real-time and therefore means we have to use the real-time
	// simulator and take the time to calculate checksums.
	//
	GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
	GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));
	Config::SetDefault ("ns3::RealtimeSimulatorImpl::SynchronizationMode", StringValue("HardLimit"));
	Config::SetDefault ("ns3::RealtimeSimulatorImpl::HardLimit", TimeValue(MilliSeconds(250)));
	
	//ns3::PacketMetadata::Enable();
	
	// Add the buildings
	if (buildingsFile.size() > 0) {
	    char inBuff[128];
	    fbuilding = fopen(buildingsFile.c_str(), "r");
	    if (fbuilding) {
	        while(fgets(inBuff, sizeof(inBuff), fbuilding)) {
    	        double xmin, xmax, ymin, ymax, zmin, zmax;
    	        sscanf(inBuff, "%lf,%lf;%lf,%lf;%lf,%lf\n", &xmin, &xmax, &ymin, &ymax, &zmin, &zmax);
    	        
    	        fprintf(stdout, "Adding building at [%s]", inBuff);
    	        
                Ptr<Building> building1 = CreateObject<Building> ();// (xmin, xmax, ymin, ymax, zmin, zmax);
				building1->SetBoundaries (Box (xmin, xmax, ymin, ymax, zmin, zmax));
                building1->SetBuildingType (Building::Residential);
                building1->SetExtWallsType (Building::ConcreteWithWindows);
                building1->SetNFloors (3);
                building1->SetNRoomsX (2);
                building1->SetNRoomsY (2);
	        }
	        fclose(fbuilding);
	    }
	}

	//
	// Create two ghost nodes.  The first will represent the virtual machine host
	// on the left side of the network; and the second will represent the VM on
	// the right side.
	//
	NodeContainer nodes;
	nodes.Create (numberUAVs);

	//
	// We're going to use 802.11 A so set up a wifi helper to reflect that.
	//
	WifiHelper wifi;// = WifiHelper::Default ();
	wifi.SetStandard (WIFI_PHY_STANDARD_80211a);
	wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate54Mbps"));

	//
	// No reason for pesky access points, so we'll use an ad-hoc network.
	//
	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
	wifiMac.SetType ("ns3::AdhocWifiMac");

	//
	// Configure the physcial layer.
	//
	/*
    Frequency: reference frequency (default 2160 MHz), note that by setting the frequency the wavelength is set accordingly automatically and viceversa).
    Lambda: the wavelength (0.139 meters, considering the above frequency).
    ShadowSigmaOutdoor: the standard deviation of the shadowing for outdoor nodes (defaul 7.0).
    ShadowSigmaIndoor: the standard deviation of the shadowing for indoor nodes (default 8.0).
    ShadowSigmaExtWalls: the standard deviation of the shadowing due to external walls penetration for outdoor to indoor communications (default 5.0).
    RooftopLevel: the level of the rooftop of the building in meters (default 20 meters).
    Los2NlosThr: the value of distance of the switching point between line-of-sigth and non-line-of-sight propagation model in meters (default 200 meters).
    ITU1411DistanceThr: the value of distance of the switching point between short range (ITU 1211) communications and long range (Okumura Hata) in meters (default 200 meters).
    MinDistance: the minimum distance in meters between two nodes for evaluating the pathloss (considered neglictible before this threshold) (default 0.5 meters).
    Environment: the environment scenario among Urban, SubUrban and OpenAreas (default Urban).
    CitySize: the dimension of the city among Small, Medium, Large (default Large).
    */
	//Ptr<HybridBuildingsPropagationLossModel> buildingPropagationLossModel = CreateObject<HybridBuildingsPropagationLossModel> ();
	//buildingPropagationLossModel->SetAttribute ("ShadowSigmaOutdoor", DoubleValue (2.0));
    //buildingPropagationLossModel->SetAttribute ("ShadowSigmaIndoor", DoubleValue (2.0));
    //propagationLossbuildingPropagationLossModelModel->SetAttribute ("ShadowSigmaExtWalls", DoubleValue (2.0));
	
	
//	YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
	YansWifiChannelHelper wifiChannel;
	wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
	wifiChannel.AddPropagationLoss("ns3::HybridRaylightBuildingsPropagationLossModel",
		//"RooftopLevel", DoubleValue(10),
		//"Environment", StringValue("OpenAreas"), 
		//"Frequency", UintegerValue (2400), 
		//"Los2NlosThr", UintegerValue (200), 
		//"ITU1411DistanceThr", UintegerValue (200),
		//"CitySize", StringValue("Large"),
		//"MinDistance", DoubleValue(0.5), 
		//"ShadowSigmaOutdoor", DoubleValue(0.0), 
		//"ShadowSigmaIndoor", DoubleValue(0.0), 
		//"ShadowSigmaExtWalls", DoubleValue(0.0),
		"FixedExternalWallsType", EnumValue(Building::StoneBlocks),
		"BuildingFileName", StringValue(buildingsFile)
	);
	//wifiChannel.AddPropagationLoss(buildingPropagationLossModel);
	
	
	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
	wifiPhy.Set("TxPowerLevels",UintegerValue(1));
    wifiPhy.Set("TxPowerStart", DoubleValue(20));
    wifiPhy.Set("TxPowerEnd", DoubleValue(20));
	wifiPhy.SetChannel (wifiChannel.Create ());
	
	
	
	
	
	
	
	
	/*
	SpectrumChannelHelper s_channelHelper = SpectrumChannelHelper::Default ();
	s_channelHelper.SetChannel ("ns3::MultiModelSpectrumChannel");
	// constant path loss added just to show capability to set different propagation loss models
	// FriisSpectrumPropagationLossModel already added by default in SpectrumChannelHelper
	//s_channelHelper.AddSpectrumPropagationLoss ("ns3::HybridBuildingsPropagationLossModel");
	Ptr<SpectrumChannel> s_channel = s_channelHelper.Create ();
	
	SpectrumWifiPhyHelper wifiPhy = SpectrumWifiPhyHelper::Default ();
	wifiPhy.SetChannel (s_channel);
	*/





	//
	// Install the wireless devices onto our ghost nodes.
	//
	NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, nodes);

	/*
	InternetStackHelper internet;
	internet.Install (nodes);

	Ipv4AddressHelper ipv4Help;
	ipv4Help.SetBase (ns3::Ipv4Address(ipBase.c_str()), "255.255.255.0");
	Ipv4InterfaceContainer interfaces = ipv4Help.Assign (devices);
	*/

	for (unsigned int i = 0; i < numberUAVs; i++) {
		char tapname_c[16];
		snprintf(tapname_c, sizeof(tapname_c), "%d", i+1);
		std::string tapName = tapBaseName + std::string(tapname_c);

		std::string mode = "UseLocal";
		TapBridgeHelper tapBridge;

		//std::string mode = "ConfigureLocal";
		//TapBridgeHelper tapBridge (interfaces.GetAddress (i));

		tapBridge.SetAttribute ("Mode", StringValue (mode));
		tapBridge.SetAttribute ("DeviceName", StringValue (tapName));
		//tapBridge.SetAttribute ("LogFileDelay", StringValue (delayFN));
		tapBridge.Install (nodes.Get (i), devices.Get (i));
	}

	wifiPhy.EnablePcapAll ("uav-connected-sim");


	/*************************** ONLY TEST!!! ****/
	/*
	MobilityHelper mobility;
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
	positionAlloc->Add (Vector (0.0, 0.0, 0.0));
	positionAlloc->Add (Vector (5.0, 0.0, 0.0));
	mobility.SetPositionAllocator (positionAlloc);
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.Install (nodes);


	MobilityHelper mobility;
	mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
			"X", StringValue ("20.0"),
			"Y", StringValue ("20.0"),
			"Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=10]"));
	mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
			                             "Mode", StringValue ("Time"),
			                             "Time", StringValue ("2s"),
			                             "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"),
			                             "Bounds", StringValue ("0|50|0|50"));
	//mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.Install (nodes);
	*/

	/*************************** ONLY TEST!!! ****/


	for (unsigned int j = 0; j < nodes.GetN(); j++) {
		Ptr<Node> pn = nodes.Get(j);
		//Ptr<Ipv4> ipv4 = pn->GetObject<Ipv4>();
		//Ipv4InterfaceAddress iaddr = ipv4->GetAddress (1,0);
		//Ipv4Address addrip = iaddr.GetLocal ();
		int sizeOfStruct = sizeof(UavLinkMobilityModel::uav_states_t);
		if (shmemStruct.compare("euler") == 0) {
			sizeOfStruct = sizeof(UavLinkMobilityModel::uav_states_euler_t);
		}

		std::ostringstream shmemname_stream;
		shmemname_stream << shmemBaseName << j;

		NS_LOG_UNCOND ("Assigning at node " << (j+1) << " the shared mem name: " << shmemname_stream.str());

		// install the mobility with the shared memory parameters
		MobilityHelper mobility;
		//mobility.SetMobilityModel ("ns3::UavLinkMobilityModel",
		//		"SharedMemName", StringValue (actsm->shmemName),
		//		"SharedMemSize", UintegerValue (actsm->shmemSize),
		//		"PositionUpdateTime", TimeValue (Time("10ms")));
		mobility.SetMobilityModel ("ns3::UavLinkMobilityModel",
				"SharedMemName", StringValue (shmemname_stream.str()),
				"SharedMemStruct", StringValue (shmemStruct),
				"SharedMemSize", UintegerValue (sizeOfStruct) );
		mobility.Install (pn);
		
	
	    BuildingsHelper::Install (pn);
	    //BuildingsHelper::MakeConsistent (mobility);
	    
	    switch (j){
	    case 0:
	        Config::ConnectWithoutContext ("/NodeList/0/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx0));
	        break;
	    case 1:
	        Config::ConnectWithoutContext ("/NodeList/1/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx1));
	        break;
	    case 2:
	        Config::ConnectWithoutContext ("/NodeList/2/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx2));
	        break;
	    case 3:
	        Config::ConnectWithoutContext ("/NodeList/3/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx3));
	        break;
	    case 4:
	        Config::ConnectWithoutContext ("/NodeList/4/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx4));
	        break;
	    case 5:
	        Config::ConnectWithoutContext ("/NodeList/5/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx5));
	        break;
	    case 6:
	        Config::ConnectWithoutContext ("/NodeList/6/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx6));
	        break;
	    case 7:
	        Config::ConnectWithoutContext ("/NodeList/7/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx7));
	        break;
	    case 8:
	        Config::ConnectWithoutContext ("/NodeList/8/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx8));
	        break;
	    case 9:
	        Config::ConnectWithoutContext ("/NodeList/9/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx9));
	        break;
	    case 10:
	        Config::ConnectWithoutContext ("/NodeList/10/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx10));
	        break;
	    case 11:
	        Config::ConnectWithoutContext ("/NodeList/11/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx11));
	        break;
	    case 12:
	        Config::ConnectWithoutContext ("/NodeList/12/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx12));
	        break;
	    case 13:
	        Config::ConnectWithoutContext ("/NodeList/13/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx13));
	        break;
	    case 14:
	        Config::ConnectWithoutContext ("/NodeList/14/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx14));
	        break;
	    case 15:
	        Config::ConnectWithoutContext ("/NodeList/15/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx15));
	        break;
	    case 16:
	        Config::ConnectWithoutContext ("/NodeList/16/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx16));
	        break;
	    case 17:
	        Config::ConnectWithoutContext ("/NodeList/17/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx17));
	        break;
	    case 18:
	        Config::ConnectWithoutContext ("/NodeList/18/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx18));
	        break;
	    case 19:
	        Config::ConnectWithoutContext ("/NodeList/19/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx19));
	        break;
	    case 20:
	        Config::ConnectWithoutContext ("/NodeList/20/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx20));
	        break;
	    default:
	        NS_LOG_UNCOND ("Too many nodes.. max 20");
	        exit(0);
	        break;
	    }
	}
	
	BuildingsHelper::MakeMobilityModelConsistent ();

    initSharedMemorySNR(nodes.GetN());

	//Config::Connect ("/NodeList/*/$ns3::MobilityModel/CourseChange", MakeCallback (&CourseChange));
	//Config::Connect ("/NodeList/*/$ns3::WifiPhy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx));
	//Config::ConnectWithoutContext ("/NodeList/0/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSnifferRx0));

	NS_LOG_UNCOND ("ALL done! Starting the simulator...");

	Simulator::Stop (Seconds (simTime));
	Simulator::Run ();

	Simulator::Destroy ();
	
	delSharedMemorySNR(nodes.GetN());
}

