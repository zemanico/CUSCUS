#!/bin/bash


checkLXCsevice(){
	printf "checking lxc-net service...   "
	ifconfig lxcbr0 &>/dev/null
	#sudo service lxc-net status | grep -q start
	if [ $? -eq 1 ]; then
		echo "${red} NO ${reset}"
		echo "Restarting lxc-net service"
		sudo service lxc-net restart
	else
		echo "${green} OK ${reset}"
	fi
}

# check and creat bridges and tap devices
checkAndCreateBridgesAndTap(){

	#lxcbr0ADDR=`ifconfig lxcbr0 | grep 'inet' | grep -v 'inet6' | cut -d: -f2 | awk '{print $2}'`
	#lxcbr0ADDR=`ip -4 addr show lxcbr0 | grep -oP "(?<=inet ).*(?=/)"`
	lxcbr0ADDR=`ip -4 addr show lxcbr0 | grep -oP "(?<=inet )[\d\.]+(?=/)"`

	printf "Using lxcbr0 address: \"$lxcbr0ADDR\". Is correct [Y/n]?"
	read answer
	if [ ! "$answer" = "" ] && [ ! "$answer" = "y" ] && [ ! "$answer" = "Y" ]; then
		printf "Insert a valid local ip address "
		read lxcbr0ADDR
	fi

	checkBridge=0
	for (( i=1; i <= $1; i++ ))
	do
		printf "Checking br-tapuav$i...   "
		sudo brctl show | grep -qwF br-tapuav$i
		if [ $? -eq 1 ]; then
			checkBridge=1
			echo "${red} NO ${reset}"
		else
			echo "${green} OK ${reset}"
			
			printf "Checking tapuav$i...   "
			ifconfig tapuav$i &>/dev/null
			if [ $? -eq 1 ]; then
				checkBridge=1
				echo "${red} NO ${reset}"
			else
				echo "${green} OK ${reset}"
				
				printf "Checking br-tapuav$i connection with tapuav$i...   "
				sudo brctl show | grep -wF br-tapuav$i | grep -qwF tapuav$i
				if [ $? -eq 1 ]; then
					checkBridge=1
					echo "${red} NO ${reset}"
				else 
					echo "${green} OK ${reset}"
				fi
			fi
		fi
	done

	if [ $checkBridge -eq 1 ]; then
		echo "WARNING: Some or all bridges/tapdev are not installed..."
	fi

	printf "You need the bridges and the TAP devices? [Y/n]?"
	read answer

	if [ "$answer" = "" ] || [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
		echo "Creating the bridges and tap devices"
	
		for (( i=1; i <= $1; i++ ))
		do
			echo "...brctl addbr br-tapuav$i"
			sudo brctl addbr br-tapuav$i
		    
			echo "...tunctl -t tapuav$i"
			sudo tunctl -t tapuav$i
		    
			echo "...tapuav$i 0.0.0.0 promisc up"
			sudo ifconfig tapuav$i 0.0.0.0 promisc up
		    
			echo "...brctl addif br-tapuav$i tapuav$i"
			sudo brctl addif br-tapuav$i tapuav$i
		    	
			echo "...ifconfig br-tapuav$i up"
			sudo ifconfig br-tapuav$i up
		done
	fi
	
	# You will also have to make sure that your kernel has ethernet filtering (ebtables, bridge-nf, arptables) disabled. If you do not do this, only STP and ARP traffic will be allowed to flow across your bridge and your whole scenario will not work. 
	printf "Disabling ethernet filtering for bridge devices... "
	for f in /proc/sys/net/bridge/bridge-nf-*; 
	do 
		sudo sh -c "echo 0 > $f"
	done
	echo "${green} OK ${reset}"
}

# check and create lxc
checkAndCreateAutoLXC(){
	for (( i=1; i <= $1; i++ ))
	do
		printf "Checking uav$i container...   "
		sudo lxc-ls -f -1 | grep -qwF uav$i
		if [ $? -eq 1 ]; then
			echo "${red} NO ${reset}"
			
			echo "Creating one LXC containers for UAV $i. It could take a long time..."
			
			# Create the configuration file
			echo "lxc.utsname = uav$i" > "/tmp/tapuav$i.conf"
			echo "lxc.network.type = veth" >> "/tmp/tapuav$i.conf"
			echo "lxc.network.flags = up" >> "/tmp/tapuav$i.conf"
			echo "lxc.network.link = br-tapuav$i" >> "/tmp/tapuav$i.conf"
			echo "lxc.network.ipv4 = $baseip.$i/24" >> "/tmp/tapuav$i.conf"
			echo "lxc.network.name = eth0" >> "/tmp/tapuav$i.conf"
			echo "lxc.network.veth.pair = veth${i}.0" >> "/tmp/tapuav$i.conf"
	
			echo "" >> "/tmp/tapuav$i.conf"
	
			echo "lxc.network.type = veth" >> "/tmp/tapuav$i.conf"
			echo "lxc.network.flags = up" >> "/tmp/tapuav$i.conf"
			echo "lxc.network.link = lxcbr0" >> "/tmp/tapuav$i.conf"
			echo "lxc.network.name = eth1" >> "/tmp/tapuav$i.conf"	
			echo "lxc.network.veth.pair = veth${i}.1" >> "/tmp/tapuav$i.conf"	
	
			echo "...lxc-create -n uav$i -t ubuntu -f /tmp/tapuav$i.conf"
			sudo lxc-create -n uav$i -t ubuntu -f /tmp/tapuav$i.conf
	
			#echo "...lxc-create -n uav$i -t none -f /tmp/tapuav$i.conf"
			#sudo lxc-create -n uav$i -t none -f /tmp/tapuav$i.conf
			
		else
			echo "${green} OK ${reset}"
		fi	
	done
	
	for (( i=1; i <= $1; i++ ))
	do
		printf "Checking uav$i running...   "
		sudo lxc-ls -f -1 --running | grep -qwF uav$i
		if [ $? -eq 1 ]; then
			echo "${red} NO ${reset}"
			
			echo "Starting the LXC containers for UAV $i..."
			
			echo "...lxc-start -n uav$i -d"
			sudo lxc-start -n uav$i -d
	
			echo "...lxc-attach -n uav$i -- dhclient eth1"
			sudo lxc-attach -n uav$i -- dhclient eth1
		else
			echo "${green} OK ${reset}"
		fi	
	done
}

cleaningProcedure(){
	
	printf "Do you whant to start the LXC-TAP-Bridge ending procedure [y/N]?"
	read answer
	if [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then

		printf "Stop containers [Y/n]?"
		read answer
		if [ "$answer" = "" ] || [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
			for (( i=1; i <= $1; i++ ))
			do
				echo "Stopping uav $i"
			
				sudo lxc-stop -n uav$i

			done
		fi

		printf "Delete containers [Y/n]?"
		read answer
		if [ "$answer" = "" ] || [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
			for (( i=1; i <= $1; i++ ))
			do
				echo "Destroying for uav $i"
			
				sudo lxc-destroy -n uav$i

			done
		fi

		printf "Delete bridges and tap devices [Y/n]?"
		read answer
		if [ "$answer" = "" ] || [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
			for (( i=1; i <= $1; i++ ))
			do
				echo "Delating tap $i"

				sudo ifconfig br-tapuav$i down

				sudo brctl delif br-tapuav$i tapuav$i

				sudo brctl delbr br-tapuav$i

				sudo ifconfig tapuav$i down

				sudo tunctl -d tapuav$i

			done
		fi


	fi
}
