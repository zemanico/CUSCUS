/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#ifndef UAV_LINK_MOBILITY_MODEL_H
#define UAV_LINK_MOBILITY_MODEL_H

#include "constant-velocity-helper.h"
#include "mobility-model.h"
#include "position-allocator.h"
#include "ns3/ptr.h"
#include "ns3/random-variable-stream.h"

#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>

namespace ns3 {

/**
 * \ingroup mobility
 * \brief Random waypoint mobility model.
 *
 * Each object starts by pausing at time zero for the duration governed
 * by the random variable "Pause".  After pausing, the object will pick
 * a new waypoint (via the PositionAllocator) and a new random speed
 * via the random variable "Speed", and will begin moving towards the
 * waypoint at a constant speed.  When it reaches the destination,
 * the process starts over (by pausing).
 *
 * This mobility model enforces no bounding box by itself; the
 * PositionAllocator assigned to this object will bound the movement.
 * If the user fails to provide a pointer to a PositionAllocator to
 * be used to pick waypoints, the simulation program will assert.
 *
 * The implementation of this model is not 2d-specific. i.e. if you provide
 * a 3d random waypoint position model to this mobility model, the model
 * will still work. There is no 3d position allocator for now but it should
 * be trivial to add one.
 */
class UavLinkMobilityModel : public MobilityModel
{
public:
	typedef struct {
		//float roll;
		//float pitch;
		//float yaw;
		float q0;
		float q1;
		float q2;
		float q3;
		float x;
		float y;
		float z;
        float wx;
        float wy;
        float wz;
        float vx;
        float vy;
        float vz;
	} uav_states_t;

	typedef struct {
		float roll;
		float pitch;
		float yaw;
		float x;
		float y;
		float z;
        float wx;
        float wy;
        float wz;
        float vx;
        float vy;
        float vz;
	} uav_states_euler_t;

	/**
	 * Register this type with the TypeId system.
	 * \return the object TypeId
	 */
	static TypeId GetTypeId (void);

protected:
	virtual void DoInitialize (void);
	virtual void DoDispose (void);
private:
	/**
	 * Update actual position, reading from the shared memory
	 */
	void UpdateActualPositionFromShMem (void);
	/**
	 * Begin current pause event, schedule future walk event
	 */
	void DoInitializePrivate (void);
	void DoInitializePrivateSharedMemory (void);
	void DoDisposePrivateSharedMemory(void);

	void PollPosition (void);

	void Quat2Euler(float q0, float q1, float q2, float q3, float &roll, float &pitch, float &yaw);

	virtual Vector DoGetPosition (void) const;
	virtual void DoSetPosition (const Vector &position);
	virtual Vector DoGetVelocity (void) const;

	ConstantVelocityHelper m_helper; //!< helper for velocity computations
	Ptr<RandomVariableStream> m_speed; //!< random variable to generate speeds
	Ptr<RandomVariableStream> m_pause; //!< random variable to generate pauses
	EventId m_event; //!< event ID of next scheduled event

	std::string shMemName;
	std::string shMemStruct;
	u_int32_t shMemSize;
	Time positionUpdateTime;

	// varaibles for the shared memory
	std::string sem_name, shm_name;
	char *mem_segment;
	int fd;
	sem_t *sem;

	Vector actualPosition;
	Vector actualVelocity;
	Vector actualOrientation;
};

} // namespace ns3

#endif /* UAV_LINK_MOBILITY_MODEL_H */
