//  created:    2011/05/01
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    exemple de code uav
//
//
/*********************************************************************/

#include "CuscusBasic.h"
#include <FrameworkManager.h>
#include <UavFactory.h>
#include <stdio.h>
#include <tclap/CmdLine.h>
#include <TargetEthController.h>

using namespace TCLAP;
using namespace std;
using namespace flair::core;
using namespace flair::meta;
using namespace flair::sensor;

string uav_type;
string log_path;
int port;
int ds3port;
string xml_file;
string name;
string broadcast;
string address;
string optiaddress;
int broadcastMS;
float l0;
float sstiff;

void parseOptions(int argc, char** argv);


int main(int argc, char* argv[]) {
    parseOptions(argc,argv);

    FrameworkManager *manager;
    manager= new FrameworkManager(name);
    manager->SetupLogger(log_path);
    manager->SetupConnection(address,port);
    manager->SetupUserInterface(xml_file);

    Uav* drone=CreateUav(name,uav_type);
    TargetEthController *controller=new TargetEthController("Dualshock3",ds3port);
    CuscusBasic* demo=new CuscusBasic(broadcast,optiaddress,controller, broadcastMS, l0, sstiff);

    demo->Start();
    demo->Join();

    delete manager;
}


void parseOptions(int argc, char** argv) {
	try {

        CmdLine cmd("Command description message", ' ', "0.1");

        ValueArg<string> nameArg("n","name","uav name, also used for vrpn",true,"x4","string");
        cmd.add( nameArg );

        ValueArg<string> typeArg("t","type","uav type: ardrone2, hds_x4, hds_x8, hds_xufo, x4_simu, x8_simu or x4_simux (with x the number of the simulated uav)",true,"hds_x4","string");
        cmd.add( typeArg );

        ValueArg<string> xmlArg("x","xml","fichier xml",true,"./reglages.xml","string");
        cmd.add( xmlArg );

        ValueArg<string> logsArg("l","logs","repertoire des logs",true,"/media/ram","string");
        cmd.add( logsArg );

        ValueArg<int> portArg("p","port","port pour station sol",true,9000,"int");
        cmd.add( portArg );

        ValueArg<string> addressArg("a","address","addresse station sol",true,"127.0.0.1","string");
        cmd.add( addressArg );

        ValueArg<int> ds3portArg("d","ds3_port","port pour ds3",false,20000,"int");
        cmd.add( ds3portArg );

        ValueArg<string> broadcastArg("b","broadcast","broadcast ip address and port,",true,"127.255.255.255:20001","string");
        cmd.add( broadcastArg );

        ValueArg<int> broadcastMSArg("m","broadcastPosMS","broadcast position time (ms)",false,1000,"int");
        cmd.add( broadcastMSArg );

        ValueArg<float> l0Arg("z","l0","desired inter-distance (m)",false,2,"float");
        cmd.add( l0Arg );

        ValueArg<string> optiAddressArg("o","optiaddress","addresse optitrack",true,"127.0.0.1:3883","string");
        cmd.add( optiAddressArg );

        ValueArg<float> sstiffArg("f","springstiffness","constant spring stiffness",false,1,"float");
        cmd.add( sstiffArg );

        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        log_path = logsArg.getValue();
        port=portArg.getValue();
        ds3port=ds3portArg.getValue();
        xml_file = xmlArg.getValue();
        name=nameArg.getValue();
        broadcast=broadcastArg.getValue();
        uav_type=typeArg.getValue();
        address=addressArg.getValue();
        broadcastMS=broadcastMSArg.getValue();
        l0=l0Arg.getValue();
        sstiff=sstiffArg.getValue();
        optiaddress=optiAddressArg.getValue();

	} catch (ArgException &e) { // catch any exceptions
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
	}
}
