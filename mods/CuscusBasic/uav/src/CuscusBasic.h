//  created:    2015/11/05
//  filename:   SimpleFleet.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo fleet
//
//
/*********************************************************************/

#ifndef CUSCUSBASIC_H
#define CUSCUSBASIC_H

#define MAXUAVNUMBER 100

#include <UavStateMachine.h>
#include <SharedMem.h>
#include <math.h>       /* hypot */
#include <map>

namespace flair {
    namespace core {
        class FrameworkManager;
        class UdpSocket;
        class AhrsData;
        class SharedMem;
    }
    namespace filter {
        class TrajectoryGenerator2DCircle;
    }
		namespace meta {
        class MetaVrpnObject;
    }
    namespace gui {
        class DoubleSpinBox;
    }
}

class UsefulTools {
public:

    static void cartesian2polar(float x, float y, float &r, float &theta) {
        r = hypot(x, y);
        theta = atan2(y, x);
    }

    static void polar2cartesian(float r, float theta, float &x, float &y) {
        x = r * cos(theta);
        y = r * sin(theta);
    }
};

class CuscusBasic : public flair::meta::UavStateMachine {
    public:

        typedef struct shmem_var {
            float snrDB;
            float signalPowDB;
        } shmem_var_t;

        typedef struct drone_info {
            flair::core::Time timestamp;
            int addr;
            shmem_var_t snr;
            float filteredSNR;
            flair::core::Vector3Df pos;
        } drone_info_t;

    public:
        CuscusBasic(std::string broadcast,std::string optiaddress,flair::sensor::TargetController *controller, int broadcastMS, float l0, float sstiffness);
        ~CuscusBasic();

    private:
        enum class BehaviourMode_t {
            Default,
            PositionHold1,
            Circle1,
            PositionHold2,
            PositionHold3,
            Circle2,
            PositionHold4,
        };

//        BehaviourMode_t orientation_state;
        BehaviourMode_t behaviourMode;
        bool vrpnLost;

        void VrpnPositionHold(void);//flight mode
        void StartCircle(void);
        void StopCircle(void);
        void ExtraTakeOff(void);
        void ExtraSecurityCheck(void);
        void ExtraCheckJoystick(void);
        const flair::core::AhrsData *GetOrientation(void) const;
        void AltitudeValues(float &z,float &dz) const;
        void PositionValues(flair::core::Vector2Df &pos_error,flair::core::Vector2Df &vel_error,float &yaw_ref);
        void PositionValues_VirtualForces(flair::core::Vector2Df &pos_error,flair::core::Vector2Df &vel_error,float &yaw_ref);
        const flair::core::AhrsData *GetReferenceOrientation(void);
        void SignalEvent(Event_t event);
        void CheckMessages(void);

        void inLoopCommands(void);
        void make1stat(flair::core::Time timeNow);
        void readSNRshmem(float &snr, float &sPower, int rcvIdx, int myIdx);

        void sendBroadcastPosition(void);
        void getNeighborhoodCenterPoint(flair::core::Vector2Df *p);
        flair::core::Vector2Df getForceFilterNodeListAcuteAngleTest(void);
        flair::core::Vector2Df getForceFilterNodeListAcuteAngleTestSNR(void);
        void checkOldNeighbourood(void);
        float calcSpringLengthFromLinkBudget(float referenceLinkBudget, float actualLinkBudget);
        double calculateAngle(flair::core::Vector2Df a, flair::core::Vector2Df b, flair::core::Vector2Df c);
        double calcDistance2D(flair::core::Vector2Df a, flair::core::Vector2Df b);

        flair::filter::Pid *u_x, *u_y;

        flair::core::Vector2Df posHold;
        float yawHold;
        flair::core::UdpSocket *message;
        flair::core::Time posWait;

        flair::filter::TrajectoryGenerator2DCircle *circle;
        flair::gui::DoubleSpinBox *xCircleCenter,*yCircleCenter,*yDisplacement;
        flair::core::AhrsData *customReferenceOrientation,*customOrientation;
		flair::meta::MetaVrpnObject *uavVrpn;

        // timer callback
        flair::core::Time callbackTimer_next;
        long int broadcastTimer;

        flair::core::Time timer1sec;

        flair::core::SharedMem *shmemSNR;
        float distanceL0;
        int myIdx;

        float distanceL0SNR;
        float alphaFilterSNR;
        float staticNodeSpringStiffness;
        double timeSecUpdateNeigh;

        std::map<int, drone_info_t> neighborhood;
};

#endif // CUSCUSBASIC_H
