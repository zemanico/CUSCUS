#!/bin/bash


#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# EXTERNAL FILE IMPORT
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

source sysCheckFUN.sh
source generateXML.sh

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# GLOBAL STATIC VARIABLES
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

PARAMETER_FILE="parametersCUSCUS.dat"
TMP_CUSCUS_DIR=/tmp/cuscus

HOLD="-hold"
#HOLD=""

bold=`tput bold`
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# FUNCTION DEFINITION
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# define usage function
usage(){
	echo "${bold}NAME${reset}"
	echo "     $0 - launch the CUSCUS simulator"
	echo ""
	echo "${bold}SYNOPSIS${reset}"
	echo "     ${bold}$0${reset} [OPTIONS]"
	echo ""
	echo "${bold}DESCRIPTION${reset}"
	echo "     CUSCUS simulator"
	echo ""
	echo "     ${bold}-p${reset}"
	echo "          parameters input file"
	echo ""
	echo "     ${bold}-h${reset}"
	echo "          print this help"
	echo ""
	echo "${bold}PARAMETERS${reset}"
	echo "     The parameter file syntax must be: PAR_NAME==PAR_VALUE"
	echo ""
	echo "     ${bold}ns3_path${reset}"
	echo "          NS-3 execution file path"
	echo ""
	echo "     ${bold}sim_path${reset}"
	echo "          world simulator execution file path"
	echo ""
	echo "     ${bold}uav_path${reset}"
	echo "          UAV executable file path"
	echo ""
	echo "     ${bold}n_UAV${reset}"
	echo "          number of UAV to simulate"
	echo ""
	echo "     ${bold}base_ip${reset}"
	echo "          UAV subnet /24; syntax: XX.YY.ZZ"
	echo ""
	echo "     [${bold}uav_type${reset}]"
	echo "          UAV type [x4,x8]; default value: x4"
	echo ""
	echo "     [${bold}uav_weight${reset}]"
	echo "          UAV weight; default value: 1.6"
	echo ""
	echo "     [${bold}flight_heigh${reset}]"
	echo "          UAV flight altitude; default value: 1"
	echo ""
	echo "     [${bold}fixed_pos_flag${reset}]"
	echo "          UAV initial position (0: random; 1: circle); default value: 0"
	echo ""
	echo "     [${bold}shm_uav_name${reset}]"
	echo "          Shared memory basename; default value: x4"
	echo ""
	echo "     [${bold}sim_time${reset}]"
	echo "          NS-3 simulation linit time; default value: 1000"
	echo ""
	echo "     [${bold}building_filename${reset}]"
	echo "          Scenario 3D model"
	echo "          This parameter must be without extension"
	echo "          There must be three different files: .ns3, .obj, .mtl"
	echo "          Example scenarios: \"default_room\" or \"default_city\""
	echo "          Default value: \"default_room\""
	echo ""
	echo "     [${bold}pos_broadcast_ms${reset}]"
	echo "          Position message broadcast interval time (ms); default value: 100"
	echo ""
	echo "     [${bold}snr_L0${reset}]"
	echo "          Reference SNR for the virtual spring algorithm; default value: 50"
	echo ""
	echo "     [${bold}spring_stiffness${reset}]"
	echo "          Static spring stiffness for the virtual spring algorithm; default value: 2"
	echo ""
	echo "${bold}AUTHORS${reset}"
	echo "     Written by ..."
	echo ""
	echo "${bold}REPORTING BUGS${reset}"
	echo "     Please send an e-mail to: cuscus@cuscus.com"
	echo ""
	echo "${bold}COPYRIGHT${reset}"
	echo "     Copyright © 2017"
	echo ""
	echo "${bold}SEE ALSO${reset}"
	echo "     Full documentation at: <http://www>"
	echo ""

	exit 1
}

parseParameter(){
	ROW_VAL=`cat ${PARAMETER_FILE} | grep "^$1=="`
	if [ "$ROW_VAL" != "" ]; then
		ACT_VAL=`echo -n ${ROW_VAL} | awk -F"^$1==" '{print $2}'`
		eval "$1=${ACT_VAL}"
		
		echo "Read parameter ${1}. Value: ${ACT_VAL}"
	fi
}

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# MAIN EXECUTION
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

. $FLAIR_ROOT/flair-dev/scripts/ubuntu_cgroup_hack.sh

#----------------------------------------------
# Parameters read & check
#----------------------------------------------

ns3_path=""
sim_path=""
uav_path=""
n_UAV=0
uav_type=x4
uav_weight=1.6
fixed_pos_flag=0
shm_uav_name=simulator-x4
sim_time=1000
pos_broadcast_ms=100
snr_L0=50
spring_stiffness=2
base_ip=""
flight_height=1

building_filename="default_room"
ns3_building_filename=""
obj_building_filename=""
mtl_building_filename=""

while getopts ":hp:" o; do
    case "${o}" in
        p)
            PARAMETER_FILE=${OPTARG}
            ;;
        h)
            usage
            ;;
        *)
            usage
            ;;
    esac
done

echo "Reading parameters..."
parseParameter ns3_path
parseParameter sim_path
parseParameter uav_path
parseParameter n_UAV
parseParameter uav_type
parseParameter uav_weight
parseParameter fixed_pos_flag
parseParameter shm_uav_name
parseParameter sim_time
parseParameter building_filename
parseParameter pos_broadcast_ms
parseParameter snr_L0
parseParameter spring_stiffness
parseParameter base_ip
parseParameter flight_height

echo "End reading parameters"
echo ""

if [ -z "${ns3_path}" ] || [ -z "${sim_path}" ] || [ -z "${uav_path}" ] || [ -z "${base_ip}" ] || [ -z "${building_filename}" ] || [ ${n_UAV} -lt 1 ]; then   
	echo "Error: please check your parameter definitions"
	echo ""
    usage
fi

if [ "${uav_type}" != "x4" ] && [ "${uav_type}" != "x8" ]; then   
	echo "Error: wrong UAV type"
	echo ""
    usage
fi

if [ "${building_filename}" != "default_room" ] && [ "${building_filename}" != "default_city" ]; then
	ns3_building_filename="${building_filename}.ns3"
	obj_building_filename="${building_filename}.obj"
	mtl_building_filename="${building_filename}.mtl"
fi

nsdir=`dirname $ns3_path`
nsexec=`basename $ns3_path`

simdir=`dirname $sim_path`
simexec=`basename $sim_path`

uavdir=`dirname $uav_path`
uavexec=`basename $uav_path`

mkdir -p $TMP_CUSCUS_DIR


#----------------------------------------------
# Checking "brctl", "lxc", and "tunctl"
#----------------------------------------------

echo -n "Checking brctl - ethernet bridge administration... "
if [ ! type "brctl" > /dev/null 2>&1 ]; then
	echo "${red} NO ${reset} - brctl needed"
	exit 1
fi
echo "${green} OK ${reset}"

echo -n "Checking lxc - linux containers... "
if [ ! type "lxc-ls" > /dev/null 2>&1 ]; then
	echo "${red} NO ${reset} - lxc containers needed. Try \"sudo apt-get install lxc\""
	exit 1
fi
echo "${green} OK ${reset}"

echo -n "Checking tunctl — create and manage persistent TUN/TAP interfaces... "
if [ ! type "tunctl" > /dev/null 2>&1 ]; then
	echo "${red} NO ${reset} tunctl needed"
	exit 1
fi
echo "${green} OK ${reset}"

echo -n "PARAMETERS CHECK... "
echo "${green} OK ${reset}"

#----------------------------------------------
# Checking and creating the main structure
#----------------------------------------------

echo "Check LXC service"

checkLXCsevice

echo "Check and Create bridges and tap devices"

checkAndCreateBridgesAndTap ${n_UAV}

echo "Check and Create LXCs"

checkAndCreateAutoLXC ${n_UAV}


#----------------------------------------------
# Compiling NS-3 before starting
#----------------------------------------------

echo "Compiling the NS-3 simulator before starting..."
cd $nsdir
./$nsexec

printf "Is the NS-3 exit status OK [Y/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
	echo "${green} OK ${reset}"
else 
	echo "${red} NO ${reset}"
	echo "Check the NS-3 code before starting"
	exit 1
fi


#-----------------------------------------------------------------------------------------------
# Start the CUSCUS simulator
#-----------------------------------------------------------------------------------------------

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# STATION SOL

printf "Press any key to start"
read -n1

echo "Starting the station sol"
xterm $HOLD -e "cd $FLAIR_ROOT/flair-bin/tools/scripts; ./launch_flairgcs.sh" &

#-----------------------------------------------------------------------------------------------

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# WORLD SIMULATOR

printf "Press any key to continue"
read -n1

echo "Starting the world simulator"

EXECWORLD=""
if [ -f /proc/xenomai/version ];then
	EXECWORLD="./${simexec}_rt"
else
	EXECWORLD="./${simexec}_nrt"
fi

#create the xml file for the simulator
reglagesFileName=${TMP_CUSCUS_DIR}/reglages.xml
if [ "$uav_type" = "x4" ]; then
	createSimulatorXML_x4 "${reglagesFileName}" ${n_UAV} ${uav_weight} ${fixed_pos_flag}
else
	createSimulatorXML_x8 "${reglagesFileName}" ${n_UAV} ${uav_weight} ${fixed_pos_flag}
fi

worldComStr=""
if [ "${building_filename}" = "default_room" ]; then
	worldComStr="$EXECWORLD -n $uav_type -t $uav_type -k ${n_UAV} -a $lxcbr0ADDR -p 9000 -x ${reglagesFileName} -o 10 -m $FLAIR_ROOT/flair-bin/models -s $FLAIR_ROOT/flair-bin/models/indoor_flight_arena.xml"
elif [ "${building_filename}" = "default_city" ]; then
	worldComStr="$EXECWORLD -n $uav_type -t $uav_type -k ${n_UAV} -a $lxcbr0ADDR -p 9000 -x ${reglagesFileName} -o 10 -m $FLAIR_ROOT/flair-bin/models -s $FLAIR_ROOT/flair-bin/models/city_tile.xml"
else
	mkdir -p ${TMP_CUSCUS_DIR}/mapmpdels
	MAPXML=$FLAIR_ROOT/flair-bin/models/newMAP.xml
	MAPBASENAME=`basename ${obj_building_filename}`
	sed "s/voliere.bsp/${MAPBASENAME}/g" $FLAIR_ROOT/flair-bin/models/indoor_flight_arena.xml > ${MAPXML}
	sed -i "s/indoor_flight_arena.zip/newMAP.zip/g" ${MAPXML}
	sed -i "s/scale x=\"10\" y=\"10\" z=\"3\"/scale x=\"100\" y=\"100\" z=\"100\"/g" ${MAPXML}
	
	cd ${TMP_CUSCUS_DIR}/mapmpdels
	mkdir -p maps
	cp $FLAIR_ROOT/flair-bin/models/indoor_flight_arena.zip $FLAIR_ROOT/flair-bin/models/newMAP.zip
	
	echo "putting: ${obj_building_filename}"
	obj_file=`basename ${obj_building_filename}`
	cp ${obj_building_filename} maps/
	zip -g $FLAIR_ROOT/flair-bin/models/newMAP.zip maps/${obj_file}
	
	echo "putting: ${mtl_building_filename}"
	mtl_file=`basename ${mtl_building_filename}`
	cp ${mtl_building_filename} maps/
	zip -g $FLAIR_ROOT/flair-bin/models/newMAP.zip maps/${mtl_file}
	
	rm -rf ${TMP_CUSCUS_DIR}/mapmpdels/maps
	cd -
	
	worldComStr="$EXECWORLD -n $uav_type -t $uav_type -k ${n_UAV} -a $lxcbr0ADDR -p 9000 -x ${reglagesFileName} -o 10 -m ${TMP_CUSCUS_DIR}/mapmpdels -s ${MAPXML}"
fi

# launch the sim
xterm $HOLD -e "cd $simdir; ${worldComStr}" &

#-----------------------------------------------------------------------------------------------

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# NS-3

printf "Press any key to continue"
read -n1

echo "Starting the network simulator (NS-3)"
ns3_map_par=""
if [ "$ns3_building_filename" != "" ]; then
	ns3_map_par="--buildingFileName=$ns3_building_filename"
fi
ns3ComStr="./$nsexec --run \"uavConnectedSimBuildingRaylight --simTime=$sim_time --nUav=$n_UAV $ns3_map_par --tapBaseName=tapuav --shmemBaseName=${shm_uav_name}_\""
echo "Executing NS-3 with the command: ${ns3ComStr}"
xterm $HOLD -e "cd $nsdir; ${ns3ComStr}" &

#-----------------------------------------------------------------------------------------------

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# UAVs

printf "Press any key to continue"
read -n1

echo "Starting the UAVs"
if [ "$n_UAV" -ge 0 ]; then
	cd $uavdir
	
	for (( u=0; u < $n_UAV; u++ ))
	do
		INDEXDRONE=${u}
		dport=$((21000+INDEXDRONE))
		broadcastMS=${pos_broadcast_ms}
		defaultSpringDistance=${snr_L0}
		sstiffness=${spring_stiffness}
	
		u1=$((INDEXDRONE+1))
		
		EXEC=""
		if [ -f /proc/xenomai/version ];then
			EXEC="./${uavexec}_rt"
		else
			EXEC="./${uavexec}_nrt"
		fi
		
		uav_Name="${uav_type}_${u}"
		uav_regles="${TMP_CUSCUS_DIR}/setup_${uav_type}.xml"
		uav_ttype="${uav_type}_simu${u}"
		
		if [ "$uav_type" = "x8" ]; then
			createUavXML_x8 ${uav_regles} ${flight_height}
		else
			createUavXML_x4 ${uav_regles} ${flight_height}
		fi
		
		commandStr="lxc-attach -n uav$u1 -e -s 'NETWORK' -- $EXEC -n ${uav_Name} -a $lxcbr0ADDR -p 9000 -l ./ -x ${uav_regles} -t ${uav_ttype} -o ${lxcbr0ADDR}:3883 -b $base_ip.255:20010 -d ${dport} -m ${broadcastMS} -z ${defaultSpringDistance} -f ${sstiffness}"		
		echo "Starting the $u1-th UAV with command \"$commandStr\""		
		sudo xterm $HOLD -e "cd $uavdir; $commandStr" &

		printf "Press any key to start next UAV"
		read -n1
	done
fi

#-----------------------------------------------------------------------------------------------

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Cleaning...

printf "Press any key to continue"
read -n1

cleaningProcedure ${n_UAV}

